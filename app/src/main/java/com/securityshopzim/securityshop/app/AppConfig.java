package com.securityshopzim.securityshop.app;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Framdev1 on 03/11/2016.
 */

public class AppConfig {
    public static final int TYPE_EMERGENCY = 100;
    public static final int TYPE_MEDICAL = 70;
    public static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());
    public static final SimpleDateFormat formatter_alt = new SimpleDateFormat("yyyyMMdd HH_mm_ss", Locale.getDefault());
    public static final String REF_EMERGENCY = "Emergency";
    public static final String REF_USERS = "User";

    public static final String[] PJM_ADMIN = new String[]{"0001", "0002", "0003", "0004", "0005", "0006", "0007", "0008", "0009", "0010"};
    public static final String[] PJM_AGENTS = new String[]{"1001", "1002", "1003", "1004", "1005", "1006", "1007", "1008", "1009", "1010"};
    public static final String[] PJM_MEDICAL = new String[]{"02001", "02002", "02003", "02004", "02005", "02006", "02007", "02008", "02009", "020010"};
    public static final String REF_TIPS = "FirstAid";
    public static final String REF_USER_AUTH = "UserAuth";
    public static final int GENDER_UNSPECIFIED = 0;
    public static final int GENDER_MALE = 1;
    public static final int GENDER_FEMALE = -1;
    public static final String REF_AGENTS = "Agents";
    public static final String REF_NOTIFICATION = "Notifications";

    public static final int USER_ADMIN = 100;
    public static final int USER_AGENT = 96;
    public static final int USER_MEDICAL = 80;
    public static final int USER_CLIENT = 1;

    public static int getUserType(String pin) {
        for (String str : PJM_ADMIN) {
            if (str.equals(pin))
                return USER_ADMIN;
        }
        for (String str : PJM_AGENTS)
            if (str.equals(pin))
                return USER_AGENT;
        for (String str : PJM_MEDICAL)
            if (str.equals(pin))
                return USER_MEDICAL;
        return USER_CLIENT;
    }
}
