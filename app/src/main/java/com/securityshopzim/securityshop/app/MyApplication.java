package com.securityshopzim.securityshop.app;

import android.app.Application;
import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Framdev1 on 03/11/2016.
 */

public class MyApplication extends Application {
    private static final String TAG = MyApplication.class.getSimpleName();
    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());
    private static boolean activityVisible;
    private static MyApplication mInstance;
    private static Context context;

    public static Context getAppContext() {
        if (MyApplication.context != null)
            return MyApplication.context;
        return (new MyApplication()).getApplicationContext();
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        MyApplication.context = getApplicationContext();

        // register with parse
    }

    public SimpleDateFormat formatter() {
        return formatter;
    }
}
