package com.securityshopzim.securityshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.helper.PrefManager;

public class SplashScreen extends AppCompatActivity {
    private static final int SPLASH_TIME_OUT = 2000;

    PrefManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pref = new PrefManager(this);

        final String pin = pref.getPin();
        if (TextUtils.isEmpty(pin)) {
            showAuthFragment();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent;
                    int type = AppConfig.getUserType(pin);
                    if (type == AppConfig.USER_ADMIN) {
                        intent = new Intent(SplashScreen.this, AdminActivity.class);
                    } else if (type == AppConfig.USER_AGENT) {
                        intent = new Intent(SplashScreen.this, EmergencyActivity.class);
                        intent.putExtra("start", type);
                    } else if (type == AppConfig.USER_MEDICAL) {
                        intent = new Intent(SplashScreen.this, EmergencyActivity.class);
                        intent.putExtra("start", type);
                    } else {
                        intent = new Intent(SplashScreen.this, MainActivity.class);
                    }
                    startActivity(intent);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void showAuthFragment() {
        startActivity(new Intent(SplashScreen.this, LoginActivity.class));
        finish();
        //FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //FragmentAuth fragment = FragmentAuth.newInstance();
        //fragment.show(ft, "auth");
    }

}
