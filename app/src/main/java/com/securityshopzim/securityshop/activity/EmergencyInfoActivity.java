package com.securityshopzim.securityshop.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.securityshopzim.securityshop.R;

public class EmergencyInfoActivity extends AppCompatActivity {

    Intent requestFor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitleEnabled(false);
        setTitle(R.string.title_activity_emergency_info);

    }

    public void emailResponse(View view) {
        try {
            String[] address = new String[]{"pm@securityshopzim.com"};
            Intent email = new Intent(Intent.ACTION_SENDTO);
            email.setData(Uri.parse("mailto:"));

            email.putExtra(Intent.EXTRA_EMAIL, address);
            email.putExtra(Intent.EXTRA_SUBJECT, "PJM Response");
            if (email.resolveActivity(getPackageManager()) != null)
                startActivity(email);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void callFire(View view) {
        String tel = "tel:+263 4 492843";
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(tel));
        makeCall(callIntent);
    }

    public void callPolice(View view) {
        String tel = "tel:+263 4 495304";
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(tel));
        makeCall(callIntent);
    }

    public void callEmergency2(View view) {
        String tel = "tel:+263 78 589 8567";
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(tel));
        makeCall(callIntent);
    }

    public void callEmergency(View view) {
        String tel = "tel:+263 71 366 3722";
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(tel));
        makeCall(callIntent);
    }

    public void callAmbulance2(View view) {
        String tel = "tel:+263 4 708 881";
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(tel));
        makeCall(callIntent);
    }

    public void callAmbulance1(View view) {
        String tel = "tel:+263 77 796 9111";
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(tel));
        makeCall(callIntent);
        //Sending emergency location to firebase
    }

    public void makeCall(Intent intent) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissionsFor(intent);
        } else {
            startActivity(intent);
        }
    }

    private void requestPermissionsFor(Intent intent) {
        requestFor = intent;
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 99);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 99) {
            boolean rejected = true;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    rejected = false;
                    break;
                } else {
                    rejected = true;
                }
            }
            if (!rejected) {
                if (requestFor != null)
                    startActivity(requestFor);
            }
        }
    }
}