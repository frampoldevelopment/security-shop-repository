package com.securityshopzim.securityshop.activity.view;


import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.securityshopzim.securityshop.activity.MapsActivity;


public class TouchableWrapper extends FrameLayout {

    public TouchableWrapper(Context context) {
        super(context);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                MapsActivity.mMapIsTouched = true;
                MapsActivity.setMapIsTouched(true);
                break;

            case MotionEvent.ACTION_UP:
                MapsActivity.mMapIsTouched = false;
                break;
        }
        return super.dispatchTouchEvent(ev);
    }
}
