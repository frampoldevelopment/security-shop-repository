package com.securityshopzim.securityshop.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.helper.PrefManager;
import com.securityshopzim.securityshop.objekt.Profile;

public class SettingsActivity extends AppCompatActivity {

    PrefManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = new PrefManager(this);

        Profile profile = pref.getCurrentUser();

        if (profile != null) {
            ((TextView) findViewById(R.id.textName)).setText(profile.getName());
            ((TextView) findViewById(R.id.textEmail)).setText(profile.getEmail());
        }
    }

    public void openAbout(View view) {
        startActivity(new Intent(this, AboutActivity.class));
    }

    public void openRateUs(View view) {
        String url = "http://www.google.com";
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    public void openShare(View view) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Hello\n\nPlease check out the Security Shop app http://www.securityshopzim.com/.\nSecurity Shop!");
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }

    public void openSettings(View view) {

    }
}
