package com.securityshopzim.securityshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.fragment.ManageUsersFragment;
import com.securityshopzim.securityshop.fragment.NewAgentFragment;
import com.securityshopzim.securityshop.fragment.NewUserFragment;
import com.securityshopzim.securityshop.fragment.NotifyFragment;
import com.securityshopzim.securityshop.helper.PrefManager;

public class AdminActivity extends AppCompatActivity {
    PrefManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pref = new PrefManager(this);
        String pin = pref.getPin();
        if (TextUtils.isEmpty(pin) || AppConfig.getUserType(pin) != AppConfig.USER_ADMIN) {
            finish();
            return;
        }

        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void addAgent(View view) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        NewAgentFragment fragment = NewAgentFragment.newInstance();
        fragment.show(ft, "new_agent");
    }

    public void addUser(View view) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        NewUserFragment fragment = NewUserFragment.newInstance();
        fragment.show(ft, "new_user");
    }

    public void manageUsers(View view) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ManageUsersFragment fragment = ManageUsersFragment.newInstance();
        fragment.show(ft, "manage_users");
    }

    public void notifyUsers(View view) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        NotifyFragment fragment = NotifyFragment.newInstance();
        fragment.show(ft, "notify_users");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_agency:
                startActivity(new Intent(AdminActivity.this, EmergencyActivity.class));
                break;
            case R.id.action_emergency:
                startActivity(new Intent(AdminActivity.this, MainActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}