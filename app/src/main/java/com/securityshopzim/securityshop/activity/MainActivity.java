package com.securityshopzim.securityshop.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.helper.OutgoingCallInterceptor;
import com.securityshopzim.securityshop.helper.PrefManager;
import com.securityshopzim.securityshop.location.GPSTracker;
import com.securityshopzim.securityshop.location.LocationProvider;
import com.securityshopzim.securityshop.objekt.EmergencyCall;
import com.securityshopzim.securityshop.objekt.Profile;
import com.securityshopzim.securityshop.util.Util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements LocationProvider.LocationCallback {

    private static final int REQUEST_CODE = 65;
    private static final String ACTION_IN = "android.intent.action.PHONE_STATE";
    private static final String ACTION_OUT = "android.intent.action.NEW_OUTGOING_CALL";
    ImageView left, right;
    Handler handler;
    Intent requestFor;
    LocationProvider locationProvider;
    Location location;
    GPSTracker gps;
    Profile profile;
    PrefManager pref;
    FirebaseDatabase database;
    private Runnable mRunnable = new Runnable() {
        public void run() {

            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_right);
            right.startAnimation(animation);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        locationProvider = new LocationProvider(this, this);
        locationProvider.connect();
        gps = new GPSTracker(this);
        database = Util.getDatabase();
        pref = new PrefManager(this);
        //putAnAuth();

        profile = pref.getCurrentUser();
        if (profile == null) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }

        gps = new GPSTracker(this);
        if (!gps.canGetLocation()) {
            gps.showSettingsAlert();
        }


        left = (ImageView) findViewById(R.id.imageLeft);
        right = (ImageView) findViewById(R.id.imageRight);

        handler = new Handler();

        //startAnimation();
    }

    public void callEmergency(View view) {
        String tel = "tel:+263 71 366 3722";
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(tel));

        //Sending emergency location to firebase

        Location location1 = gps.getSavedLocation();
        if (location1 != null) {
            this.location = location1;
            new SendEmergencyCallAsync().execute(AppConfig.TYPE_EMERGENCY);
            makeCall(callIntent);
        } else {
            if (gps.canGetLocation()) {
                if (location != null) {
                    new SendEmergencyCallAsync().execute(AppConfig.TYPE_EMERGENCY);
                }
                makeCall(callIntent);
            } else {
                gps.showSettingsAlert();
            }
        }
    }

    public void callEmergency2(View view) {
        String tel = "tel:+ 263 78 589 8567";
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(tel));

        //Sending emergency location to firebase

        Location location1 = gps.getSavedLocation();
        if (location1 != null) {
            this.location = location1;
            new SendEmergencyCallAsync().execute(AppConfig.TYPE_EMERGENCY);
            makeCall(callIntent);
        } else {
            if (gps.canGetLocation()) {
                if (location != null) {
                    new SendEmergencyCallAsync().execute(AppConfig.TYPE_EMERGENCY);
                }
                makeCall(callIntent);
            } else {
                gps.showSettingsAlert();
            }
        }
    }

    public void makeCall(Intent intent) {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissionsFor(intent);
        } else {
            final IntentFilter filter = new IntentFilter();
            filter.addAction(ACTION_OUT);
            //filter.addAction(ACTION_IN);
            this.registerReceiver(new OutgoingCallInterceptor(100), filter);
            startActivity(intent);
        }
    }

    public void callAmbulance(View view) {
        String tel = "tel:+263 77 796 9111"; //original number, return this number before publishing
        //String tel = "tel:+263 783 106 331"; //this is Ravi's phone, remove it before publishing
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(tel));
        //makeCall(callIntent);

        Location location1 = gps.getSavedLocation();
        if (location1 != null) {
            this.location = location1;
            new SendEmergencyCallAsync().execute(AppConfig.TYPE_MEDICAL);
            makeCall(callIntent);
        } else {
            if (gps.canGetLocation()) {
                if (location != null) {
                    new SendEmergencyCallAsync().execute(AppConfig.TYPE_MEDICAL);
                }
                makeCall(callIntent);
            } else {
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void requestPermissionsFor(Intent intent) {
        requestFor = intent;
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 99);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 99) {
            boolean rejected = true;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    rejected = false;
                    break;
                } else {
                    rejected = true;
                }
            }
            if (!rejected) {
                if (requestFor != null)
                    startActivity(requestFor);
            }
        }
    }

    public void emergencyTipsClick(View view) {
        Intent intent = new Intent(this, EmergencyTipsActivity.class);
        startActivity(intent);
    }

    public void notificationClick(View view) {
        Intent intent = new Intent(this, NotificationActivity.class);
        startActivity(intent);
    }

    public void emergencyInfoClick(View view) {
        Intent intent = new Intent(this, EmergencyInfoActivity.class);
        startActivity(intent);
    }

    private void startAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_left);
        left.startAnimation(startAnimation);

        runCurrentHandler();

    }

    private void runCurrentHandler() {
        handler = new Handler();
        handler.postDelayed(mRunnable, 1000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem admin = menu.findItem(R.id.action_admin);
        MenuItem agent = menu.findItem(R.id.action_testimonial);

        if (pref == null)
            pref = new PrefManager(MainActivity.this);
        String pin = pref.getPin();
        int type = AppConfig.getUserType(pin);
        if (admin != null) {
            if (type == AppConfig.USER_ADMIN)
                admin.setVisible(true);
            else
                admin.setVisible(false);
        }
        if (agent != null) {
            if (type == AppConfig.USER_ADMIN || type == AppConfig.USER_AGENT)
                agent.setVisible(true);
            else
                agent.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            return true;
        } else if (id == R.id.action_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Hello\n\nPlease check out the Security Shop app http://www.securityshopzim.com/.\nSecurity Shop!");
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
        } else if (id == R.id.action_profile) {
            startActivity(new Intent(this, ProfileActivity.class));
        } else if (id == R.id.action_feedback) {
            //startActivity(new Intent(this, AuthActivity.class));
        } else if (id == R.id.action_testimonial) {
            startActivity(new Intent(this, EmergencyActivity.class));
        } else if (id == R.id.action_logout) {

            AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
            adb.setTitle("Signing out");
            adb.setMessage("You are about to sign out of Response.\r\n\r\nContinue?");
            adb.setPositiveButton("Sign Out", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    profile = null;
                    pref.setUserUID("");
                    pref.savePin("");
                    pref.deleteUser();
                    Intent intent = new Intent(MainActivity.this, SplashScreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(MainActivity.this, "Successfully logged out!", Toast.LENGTH_LONG).show();
                    finish();
                }
            });
            adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog d = adb.create();
            d.setCanceledOnTouchOutside(true);
            d.show();

        } else if (id == R.id.action_admin) {
            startActivity(new Intent(MainActivity.this, AdminActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void handleNewLocation(Location location) {
        this.location = location;
    }

    class SendEmergencyCallAsync extends AsyncTask<Integer, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Integer... integers) {
            try {
                String date = AppConfig.formatter.format(new Date());
                String user = "-KTiDjgrJXb6BAlx83bE";
                String name = "Unknown caller";
                String phone = "Phone details";
                if (profile != null) {
                    user = profile.getUid();
                    name = profile.getName();
                    phone = profile.getPhone();
                }
                int type = integers[0];
                String gps = location.getLatitude() + "," + location.getLongitude();
                DatabaseReference reference = database.getReference(AppConfig.REF_EMERGENCY);

                String key = reference.push().getKey();
                EmergencyCall call = new EmergencyCall(key, date, user, name, type, gps, phone, 0);

                Map<String, Object> callValues = call.toMap();

                Map<String, Object> childValues = new HashMap<>();
                childValues.put("/" + key, callValues);

                reference.updateChildren(childValues);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

}
