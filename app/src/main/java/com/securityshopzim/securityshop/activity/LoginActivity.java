package com.securityshopzim.securityshop.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.helper.PrefManager;
import com.securityshopzim.securityshop.objekt.Profile;
import com.securityshopzim.securityshop.util.Util;

public class LoginActivity extends AppCompatActivity {
    TextInputLayout layoutPin, layoutPassword;
    LinearLayout progressBar;
    TextInputEditText editPin, editPassword;
    int op = 0;

    FirebaseDatabase database;
    PrefManager pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        database = Util.getDatabase();
        pref = new PrefManager(this);

        layoutPin = (TextInputLayout) findViewById(R.id.layoutPin);
        layoutPassword = (TextInputLayout) findViewById(R.id.layoutPassword);

        progressBar = (LinearLayout) findViewById(R.id.progressLayout);

        editPassword = (TextInputEditText) findViewById(R.id.password);
        editPin = (TextInputEditText) findViewById(R.id.editPin);

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                op = -1;

                authenticatePIN();
            }
        });
        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
            }
        });

    }

    private void authenticatePIN() {
        final String pin = editPin.getText().toString();
        final String password = editPassword.getText().toString();

        int result = 0;
        if (TextUtils.isEmpty(pin)) {
            result++;
            layoutPin.setError("Security shop PIN required!");
        }
        if (TextUtils.isEmpty(password)) {
            result++;
            layoutPassword.setError("Password required");
        }
        if (result != 0) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        DatabaseReference reference = database.getReference(AppConfig.REF_USERS);
        Query q = reference.orderByChild("securityShopId").equalTo(pin);
        q.keepSynced(true);

        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.GONE);
                if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        Profile profile = child.getValue(Profile.class);
                        if (profile.getPassword().equals(password)) {
                            pref.savePin(pin);
                            pref.saveCurrentUser(profile);
                            pref.setUserUID(profile.getUid());

                            int type = AppConfig.getUserType(pin);
                            Intent intent;
                            if (type == AppConfig.USER_ADMIN) {
                                intent = new Intent(LoginActivity.this, AdminActivity.class);
                            } else if (type == AppConfig.USER_AGENT) {
                                intent = new Intent(LoginActivity.this, EmergencyActivity.class);
                                intent.putExtra("start", type);
                            } else if (type == AppConfig.USER_MEDICAL) {
                                intent = new Intent(LoginActivity.this, EmergencyActivity.class);
                                intent.putExtra("start", type);
                            } else {
                                intent = new Intent(LoginActivity.this, MainActivity.class);
                            }
                            startActivity(intent);
                            finish();

                            return;
                        }
                    }
                    showInvalidPasswordAlert();
                } else {
                    showNotFoundAlert();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

       /* DatabaseReference ref = database.getReference(AppConfig.REF_USER_AUTH);
        Query query = ref.orderByChild("pin").equalTo(pin);
        query.keepSynced(true);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.GONE);
                if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        UserAuth auth = child.getValue(UserAuth.class);
                        String pass = auth.getPassword();
                        if (pass == null) {
                            showNotFoundAlert();
                        } else {
                            pref.savePin(pin);
                            if (pass.equals(password)) {
                                int type = AppConfig.getUserType(pin);
                                Intent intent;
                                if (type == AppConfig.USER_ADMIN) {
                                    intent = new Intent(LoginActivity.this, AdminActivity.class);
                                } else if (type == AppConfig.USER_AGENT) {
                                    intent = new Intent(LoginActivity.this, EmergencyActivity.class);
                                    intent.putExtra("start", type);
                                } else if (type == AppConfig.USER_MEDICAL) {
                                    intent = new Intent(LoginActivity.this, EmergencyActivity.class);
                                    intent.putExtra("start", type);
                                } else {
                                    intent = new Intent(LoginActivity.this, MainActivity.class);
                                }
                                startActivity(intent);
                                finish();
                            } else {
                                showInvalidPasswordAlert();
                            }
                        }
                    }
                } else {
                    showNotFoundAlert();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
    }

    private void showInvalidPasswordAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Incorrect password");
        builder.setMessage("The password or PIN number you entered was not correct.\r\n\r\nPlease try again");
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void showNotFoundAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Invalid PIN");
        builder.setMessage("The PIN number you entered did not match any Security Shop PINs.\r\n\r\nPlease check your pin and try again or contact Security shop for help");
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

}
