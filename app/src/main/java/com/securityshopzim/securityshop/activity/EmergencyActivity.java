package com.securityshopzim.securityshop.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.fragment.EmergencyFragment;
import com.securityshopzim.securityshop.helper.PrefManager;

import java.util.ArrayList;
import java.util.List;

public class EmergencyActivity extends AppCompatActivity {

    PrefManager pref;
    int start = AppConfig.USER_AGENT;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pref = new PrefManager(this);

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            start = extra.getInt("start");
        }

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        EmergencyFragment security = EmergencyFragment.getInstance();
        Bundle extra = new Bundle();
        extra.putInt("type", AppConfig.TYPE_EMERGENCY);
        boolean showEmergency = shouldShowEmergency();
        extra.putBoolean("shouldShow", showEmergency);
        security.setArguments(extra);
        adapter.addFragment(security, "SECURITY");

        EmergencyFragment medical = EmergencyFragment.getInstance();
        Bundle extra2 = new Bundle();
        extra2.putInt("type", AppConfig.TYPE_MEDICAL);
        extra2.putBoolean("shouldShow", shouldShowMedical());
        medical.setArguments(extra2);
        adapter.addFragment(medical, "MEDICAL");

        viewPager.setAdapter(adapter);

        if (start == AppConfig.USER_MEDICAL)
            viewPager.setCurrentItem(1);
        else
            viewPager.setCurrentItem(0);
    }

    private boolean shouldShowEmergency() {
        String pin = pref.getPin();
        int type = AppConfig.getUserType(pin);

        return (type == AppConfig.USER_ADMIN || type == AppConfig.USER_AGENT);
    }

    private boolean shouldShowMedical() {
        String pin = pref.getPin();

        int type = AppConfig.getUserType(pin);

        return (type == AppConfig.USER_ADMIN || type == AppConfig.USER_MEDICAL);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
