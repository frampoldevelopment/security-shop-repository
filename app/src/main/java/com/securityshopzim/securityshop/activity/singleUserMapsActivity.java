package com.securityshopzim.securityshop.activity;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.ui.IconGenerator;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.activity.view.EmergencyIconRendered;
import com.securityshopzim.securityshop.objekt.ClientLocation;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.app.MyApplication;
import com.securityshopzim.securityshop.helper.PrefManager;
import com.securityshopzim.securityshop.location.GPSTracker;
import com.securityshopzim.securityshop.objekt.ClusterMarkerItem;
import com.securityshopzim.securityshop.objekt.EmergencyCall;
import com.securityshopzim.securityshop.objekt.Wrapper;
import com.securityshopzim.securityshop.util.Util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class singleUserMapsActivity extends FragmentActivity implements OnMapReadyCallback{

    public static boolean mMapIsTouched = false;
    static CardView cardView;
    static Animation hideInfoWindow;
    static boolean WINDOW_STATUS = false;
    ClusterMarkerItem clickedClusterItem;
    Animation showInfoWindow;
    Location location;
    GPSTracker gps;
    FirebaseDatabase database;
    List<EmergencyCall> calls;
    List<ClusterMarkerItem> items;
    TextView cardCaller, cardMobile, cardTime;
    Wrapper wrapper;
    private GoogleMap mMap;
    private boolean locationChanged = false;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    ClusterManager<ClusterMarkerItem> mClusterManager;
    Cluster<ClusterMarkerItem> clickedCluster;
    String [] myGPS, myGPS1;

    public static void setMapIsTouched(boolean isTouched) {
        if (WINDOW_STATUS == true) {
            WINDOW_STATUS = false;
            cardView.startAnimation(hideInfoWindow);
            cardView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        checkPlayServices();

        gps = new GPSTracker(this);
        database = Util.getDatabase();
        calls = new ArrayList<>();
        items = new ArrayList<>();

        if (!gps.canGetLocation()) {
            gps.showSettingsAlert();
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            wrapper = (Wrapper) bundle.getSerializable("wrapper");
        }

        cardView = (CardView) findViewById(R.id.info_window);
        cardView.setVisibility(View.INVISIBLE);
        cardCaller = (TextView) findViewById(R.id.emergency_caller);
        cardMobile = (TextView) findViewById(R.id.emergency_contact);
        cardTime = (TextView) findViewById(R.id.emergency_time);
        WINDOW_STATUS = false;

        showInfoWindow = AnimationUtils.loadAnimation(com.securityshopzim.securityshop.activity.singleUserMapsActivity.this, R.anim.info_window_show);
        hideInfoWindow = AnimationUtils.loadAnimation(com.securityshopzim.securityshop.activity.singleUserMapsActivity.this, R.anim.info_window_hide);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }

    private void firebaseEmergencyData() {
        if (wrapper != null) {
            for (EmergencyCall call : wrapper.getCalls()) {
                addEmergencyItem(call);
            }
            return;
        }
        DatabaseReference reference = database.getReference(AppConfig.REF_EMERGENCY);

        Query query = reference.orderByChild("uid").equalTo(0);
        query.keepSynced(true);

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                EmergencyCall emergencyCall = dataSnapshot.getValue(EmergencyCall.class);
                PrefManager pref = new PrefManager(singleUserMapsActivity.this);
                pref.addEmergency(emergencyCall.getUid());
                addEmergencyItem(emergencyCall);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                EmergencyCall emergencyCall = dataSnapshot.getValue(EmergencyCall.class);
                addEmergencyItem(emergencyCall);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addEmergencyItem(EmergencyCall call) {
        int callStatus = getCallStatus(call);
        if (callStatus == 0) {
            calls.add(call);
        }
        if (callStatus < 2) {
            double lat, lng;
            String[] gps = call.getLocation().split(",");
            if (gps.length == 2) {
                lat = Double.parseDouble(gps[0]);
                lng = Double.parseDouble(gps[1]);

                //consider calling more marker information from this tag line here. its a great start
                ClusterMarkerItem offsetItem = new ClusterMarkerItem(call.getUid(), lat, lng, call.getDate(), call.getCaller(), call.getRead());
                items.add(offsetItem);
                mClusterManager.addItem(offsetItem);
                mClusterManager.cluster();

            }
            //notifySecurityShopAgent(call);
        }
    }

    private int getCallStatus(EmergencyCall call) {
        for (EmergencyCall x : calls) {
            if (x.getUid().equals(call.getUid())) {
                for (ClusterMarkerItem item : items) {
                    if (item.getUid().equals(call.getUid())) {
                        return 2;
                    }
                }
                return 1;
            }
        }
        return 0;
    }

    private int maydayLocation(EmergencyCall call) {
        for (EmergencyCall x : calls) {
            if (x.getUid().equals(call.getUid())) {
                for (ClusterMarkerItem item : items) {
                    if (item.getUid().equals(call.getUid())) {
                        return 2;
                    }
                }
                return 1;
            }
        }
        return 0;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        myGPS1 = maydayLocation();
        double lat = Double.parseDouble(myGPS1[0]);
        double lng = Double.parseDouble(myGPS1[1]);

        if (lat == -1 && lng == -1) {
            if (location != null) {
                lat = location.getLatitude();
                lng = location.getLongitude();
            }
        }
        if (lat != -1 && lng != -1) {
            LatLng sydney = new LatLng(lat, lng);
            locationChanged = true;
            //mMap.addMarker(new MarkerOptions().position(sydney).title("Emergency Marker"));

            IconGenerator iconFactory = new IconGenerator(singleUserMapsActivity.this);
            iconFactory.setStyle(IconGenerator.STYLE_GREEN);
            MarkerOptions markerOptions = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon("Harare")))
                    .position(sydney)
                    .anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());
            mMap.addMarker(markerOptions);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        }

        //Next four lines zoom in the map and put focus on the current marker
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(lat, lng));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);

        setUpCluster(lat, lng);
        firebaseEmergencyData();
    }

    private void setUpCluster(double latitude, double longitude) {

        // Position the map.
        //double lat = gps.getLatitude();
        //double lng = gps.getLongitude();

        double lat = latitude;
        double lng = longitude;
        if (lat == -1 && lng == -1) {
            if (location != null) {
                lat = location.getLatitude();
                lng = location.getLongitude();
            }
        }
        if (lat != -1 && lng != -1) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 10));
            locationChanged = true;
        }

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        mClusterManager = new ClusterManager<>(this, mMap);

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new CustomInfoWindowAdapter());

        mClusterManager.setRenderer(new EmergencyIconRendered(singleUserMapsActivity.this, mMap, mClusterManager));

        //ClusterMarkerItem offsetItem = new ClusterMarkerItem("", clat, clng, "Clicked", "Clicked item", 0);
        //mClusterManager.addItem(offsetItem);

        // Add cluster items (markers) to the cluster manager.
        //addItems();

        mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<ClusterMarkerItem>() {
            @Override
            public boolean onClusterClick(Cluster<ClusterMarkerItem> cluster) {
                clickedCluster = cluster;
                Toast.makeText(singleUserMapsActivity.this, "Cluster clicked: " + cluster.getSize(), Toast.LENGTH_SHORT).show();
                LatLngBounds.Builder builder = LatLngBounds.builder();
                for (ClusterMarkerItem item : cluster.getItems()) {
                    builder.include(item.getPosition());
                }
                final LatLngBounds bounds = builder.build();
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                return true;
            }
        });
        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<ClusterMarkerItem>() {
            @Override
            public boolean onClusterItemClick(ClusterMarkerItem clusterMarkerItem) {

                clickedClusterItem = clusterMarkerItem;
                return false;
            }
        });
        mClusterManager.setOnClusterItemInfoWindowClickListener(new ClusterManager.OnClusterItemInfoWindowClickListener<ClusterMarkerItem>() {
            @Override
            public void onClusterItemInfoWindowClick(ClusterMarkerItem clusterMarkerItem) {
                WINDOW_STATUS = true;
                cardView.startAnimation(showInfoWindow);
                cardView.setVisibility(View.VISIBLE);


                EmergencyCall call = getEmergencyCall(clusterMarkerItem.getUid());
                if (call != null && call.getRead() == 0) {
                    callIsRead(call.getUid(), 1);

                    refreshClusterItems(call);
                }
                cardCaller.setText(call == null ? clusterMarkerItem.getTitle() : call.getCaller());
                cardMobile.setText(call == null ? clusterMarkerItem.getUid() : call.getPhone());
                if (call != null) {
                    try {
                        Date p = (new MyApplication()).formatter().parse(call.getDate());
                        CharSequence cs = DateUtils.getRelativeTimeSpanString(p.getTime(), (new Date()).getTime(), DateUtils.DAY_IN_MILLIS);
                        cardTime.setText(cs);

                    } catch (ParseException e) {
                        e.printStackTrace();
                        cardTime.setText(call.getLocation());
                    }
                } else {
                    cardTime.setText(clusterMarkerItem.getSnippet());
                }
                ((TextView) findViewById(R.id.emergency_time)).setText(call == null ? "Emergency Call" : call.getType() == AppConfig.TYPE_EMERGENCY ? "Emergency Call" : "Ambulance requested");
            }
        });
        mClusterManager.setOnClusterInfoWindowClickListener(new ClusterManager.OnClusterInfoWindowClickListener<ClusterMarkerItem>() {
            @Override
            public void onClusterInfoWindowClick(Cluster<ClusterMarkerItem> cluster) {

            }
        });
    }

    private EmergencyCall getEmergencyCall(String uid) {
        for (EmergencyCall call : calls) {
            if (call.getUid().equals(uid))
                return call;
        }
        return null;
    }

    private void refreshClusterItems(EmergencyCall cur) {
        mMap.clear();
        mClusterManager.clearItems();
        List<ClusterMarkerItem> items = new ArrayList<>();
        for (EmergencyCall call : calls) {
            double lat = 0;
            double lng = 0;
            String[] gps = call.getLocation().split(",");
            if (gps.length == 2) {
                lat = Double.parseDouble(gps[0]);
                lng = Double.parseDouble(gps[1]);


                int read = call.getRead();

                if (cur.getUid().equals(call.getUid()))
                    read = 1;
                ClusterMarkerItem offsetItem = new ClusterMarkerItem(call.getUid(), lat, lng, call.getDate(), call.getCaller(), read);
                items.add(offsetItem);
                mClusterManager.addItem(offsetItem);
                mClusterManager.cluster();

            }
        }
    }

    private void callIsRead(String uid, int read) {
        DatabaseReference reference = database.getReference(AppConfig.REF_EMERGENCY);
        reference.child(uid).child("read").setValue(read);
    }

    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        private View view;

        public CustomInfoWindowAdapter() {
            if (clickedClusterItem != null && clickedClusterItem.getRead() == 0) {
                view = getLayoutInflater().inflate(R.layout.custom_info_window_inread, null);
            } else {
                view = getLayoutInflater().inflate(R.layout.custom_info_window, null);
            }
        }

        @Override
        public View getInfoWindow(Marker marker) {
            if (clickedClusterItem != null) {
                if (clickedClusterItem.getRead() == 0) {
                    view = getLayoutInflater().inflate(R.layout.custom_info_window_inread, null);
                } else {
                    view = getLayoutInflater().inflate(R.layout.custom_info_window, null);
                }
            }
            ImageView image = ((ImageView) view.findViewById(R.id.badge));
            TextView title = ((TextView) view.findViewById(R.id.title));
            TextView snippet = ((TextView) view.findViewById(R.id.snippet));
            if (clickedClusterItem != null) {
                title.setText(clickedClusterItem.getTitle());
                snippet.setText(clickedClusterItem.getSnippet());
            }
            return view;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    }

    private String[] maydayLocation() {
        if (wrapper != null) {
            for (EmergencyCall call : wrapper.getCalls()) {
                myGPS = x(call);
            }
            return myGPS;
        }
        return myGPS;
    }

    public String[] x(EmergencyCall call){
        DatabaseReference reference = database.getReference(AppConfig.REF_EMERGENCY);

        Query query = reference.orderByChild("uid").equalTo(0);
        query.keepSynced(true);
        String[] gps = call.getLocation().split(",");
        if (gps.length == 2) {
            double lat = Double.parseDouble(gps[0]);
            double lng = Double.parseDouble(gps[1]);
            Log.i("Developer Custorm Info", "Latitude is: " + lat);
            Log.i("Developer Custorm Info", "Longitude is: " + lng);
        }
        return gps;
    }
}