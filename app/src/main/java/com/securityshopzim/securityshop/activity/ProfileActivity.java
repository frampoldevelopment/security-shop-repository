package com.securityshopzim.securityshop.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.ViewTarget;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.helper.PrefManager;
import com.securityshopzim.securityshop.objekt.Profile;
import com.securityshopzim.securityshop.objekt.UserAuth;
import com.securityshopzim.securityshop.util.Util;

import co.lujun.androidtagview.TagContainerLayout;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    TextView textName, textEmail, textPhone, textAddress, textSecurityId, textBirthday, textMore, textAccount;
    TagContainerLayout tagAllergies;
    Button btnUpload;
    CircleImageView circleImageView;

    Profile profile;
    PrefManager pref;
    FirebaseDatabase database;
    FirebaseAuth auth;
    FirebaseStorage storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        database = Util.getDatabase();
        storage = FirebaseStorage.getInstance();
        auth = FirebaseAuth.getInstance();
        pref = new PrefManager(this);

        declareVariables();

        getUserProfile();

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileActivity.this, RegisterActivity.class));
            }
        });
    }

    private void getUserProfile() {
        profile = pref.getCurrentUser();
        setValues();
        String user = "";
        if (profile != null)
            user = profile.getUid();
        else {
            startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
            finish();
        }
        if (!TextUtils.isEmpty(user)) {
            DatabaseReference reference = database.getReference(AppConfig.REF_USERS);
            Query query = reference.orderByChild("uid").equalTo(user);
            query.keepSynced(true);

            query.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    profile = dataSnapshot.getValue(Profile.class);
                    pref.setUserUID(profile.getUid());
                    pref.saveCurrentUser(profile);
                    setValues();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    profile = dataSnapshot.getValue(Profile.class);
                    pref.setUserUID(profile.getUid());
                    pref.saveCurrentUser(profile);
                    setValues();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    profile = dataSnapshot.getValue(Profile.class);
                    pref.deleteUser();
                    profile = null;
                    final DatabaseReference ref = database.getReference(AppConfig.REF_USER_AUTH);
                    Query query1 = ref.orderByChild("email").equalTo(profile.getEmail());
                    query1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot != null) {
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    UserAuth auth = child.getValue(UserAuth.class);
                                    ref.child(auth.getUid()).removeValue();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void setValues() {
        if (profile != null) {
            textName.setText(profile.getName());
            textEmail.setText(profile.getEmail());
            textAddress.setText(profile.getAddress());
            textPhone.setText(profile.getPhone());
            textSecurityId.setText("PIN: " + profile.getSecurityShopId());
            textBirthday.setText("Born: " + profile.getBirthday());
            textMore.setText("Medical Aid:\t\t" + profile.getMedicalAidName());
            textAccount.setText("Account:\t\t\t" + profile.getMedicalAidNumber());

            String x = profile.getAllergies();
            if (!TextUtils.isEmpty(x)) {
                String[] y = x.split("#");
                tagAllergies.setTags(y);
            }

            String url = profile.getImage();
            if (!TextUtils.isEmpty(url)) {
                try {
                    Glide.with(ProfileActivity.this)
                            .load(url)
                            .asBitmap()
                            .thumbnail(0.5f)
                            .placeholder(R.drawable.default_contact)
                            .error(R.drawable.default_contact)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(new ViewTarget<CircleImageView, Bitmap>(circleImageView) {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    CircleImageView img = this.view;
                                    img.setImageBitmap(resource);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void declareVariables() {
        textName = (TextView) findViewById(R.id.profile_full_name);
        textEmail = (TextView) findViewById(R.id.profile_email);
        textPhone = (TextView) findViewById(R.id.profile_phone);
        textAddress = (TextView) findViewById(R.id.profile_address);
        textSecurityId = (TextView) findViewById(R.id.profile_security_shop_id);
        textBirthday = (TextView) findViewById(R.id.profile_dob);
        textMore = (TextView) findViewById(R.id.profile_medical_aid);
        textAccount = (TextView) findViewById(R.id.profile_medical_aid_account);

        tagAllergies = (TagContainerLayout) findViewById(R.id.allergies);

        btnUpload = (Button) findViewById(R.id.btnUpload);
        circleImageView = (CircleImageView) findViewById(R.id.profile_image);
    }

}
