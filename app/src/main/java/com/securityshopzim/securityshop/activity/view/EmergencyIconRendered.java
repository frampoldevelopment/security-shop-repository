package com.securityshopzim.securityshop.activity.view;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.securityshopzim.securityshop.objekt.ClusterMarkerItem;


/**
 * Created by Framdev1 on 19/09/2016.
 */
public class EmergencyIconRendered extends DefaultClusterRenderer<ClusterMarkerItem> {
    Context context;
    GoogleMap map;
    ClusterManager<ClusterMarkerItem> clusterManager;

    public EmergencyIconRendered(Context context, GoogleMap map, ClusterManager<ClusterMarkerItem> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;
        this.map = map;
        this.clusterManager = clusterManager;
    }

    @Override
    protected void onBeforeClusterItemRendered(ClusterMarkerItem item, MarkerOptions markerOptions) {
        BitmapDescriptor descriptor = item.getIconDescriptor();
        if (descriptor != null) {
            markerOptions.icon(descriptor);
        }
        markerOptions.snippet(item.getSnippet());
        markerOptions.title(item.getTitle());
        super.onBeforeClusterItemRendered(item, markerOptions);
    }
}
