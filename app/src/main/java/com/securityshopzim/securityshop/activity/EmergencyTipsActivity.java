package com.securityshopzim.securityshop.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.adapter.EmergencyTipAdapter;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.objekt.EmergencyTip;
import com.securityshopzim.securityshop.util.Util;

import java.util.ArrayList;
import java.util.List;

public class EmergencyTipsActivity extends AppCompatActivity {

    SpinKitView spinKitView;
    RecyclerView recyclerView;
    List<EmergencyTip> data;
    EmergencyTipAdapter adapter;
    LinearLayoutManager layoutManager;

    FirebaseDatabase database;

    boolean visible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_tips);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        database = Util.getDatabase();

        spinKitView = (SpinKitView) findViewById(R.id.spinKitView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(EmergencyTipsActivity.this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setVisibility(View.GONE);
        spinKitView.setVisibility(View.VISIBLE);
        visible = false;

        data = new ArrayList<>();
        firebaseGetTips();

        adapter = new EmergencyTipAdapter(EmergencyTipsActivity.this, data);
        recyclerView.setAdapter(adapter);
    }

    private void firebaseGetTips() {
        DatabaseReference ref = database.getReference(AppConfig.REF_TIPS);
        Query query = ref.orderByKey();
        query.keepSynced(true);

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (!visible) {
                    recyclerView.setVisibility(View.VISIBLE);
                    spinKitView.setVisibility(View.GONE);
                    visible = true;
                }
                EmergencyTip tip = dataSnapshot.getValue(EmergencyTip.class);
                adapter.addItem(tip);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                EmergencyTip tip = dataSnapshot.getValue(EmergencyTip.class);
                adapter.addItem(tip);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}
