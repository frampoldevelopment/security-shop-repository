package com.securityshopzim.securityshop.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.target.ViewTarget;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.app.MyApplication;
import com.securityshopzim.securityshop.helper.PrefManager;
import com.securityshopzim.securityshop.objekt.Profile;
import com.securityshopzim.securityshop.objekt.UserAuth;
import com.securityshopzim.securityshop.util.Util;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterActivity extends AppCompatActivity {
    private static final int ALLERGIES_REQUEST_CODE = 67;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "Camera";
    private static final int IMAGE_REQUEST_CODE = 100;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    TextInputLayout layoutName, layoutEmail, layoutPhone, layoutAddress, layoutShopId, layoutDob, layoutMedicalAidName, layoutMedicalAidNumber, layoutAllergy, layoutPassword, layoutConfirmPassword;
    TextInputEditText textName, textEmail, textPhone, textAddress, textShopId, editDob, editMedicalAidName, editMedicalAidNumber, editAllergy, editPassword, editConfirmPassword;
    Button btnUpload, addAllergy;
    CircleImageView imgProfile;
    RadioButton radioMale, radioFemale;
    TagContainerLayout tagAllergies;

    Profile profile;
    PrefManager pref;
    FirebaseDatabase database;
    FirebaseAuth auth;
    FirebaseStorage storage;

    String date;
    String url;
    List<String> allergies;
    private Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        database = Util.getDatabase();
        auth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        pref = new PrefManager(this);

        allergies = new ArrayList<>();

        checkPermissions();


        setupViews();
        getCurrentUser();

        tagAllergies.setTags(allergies);

        tagAllergies.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                Toast.makeText(RegisterActivity.this, "Long press to remove allergy", Toast.LENGTH_LONG).show();
            }

            public void onTagCrossClick (int x){

            }

            @Override
            public void onTagLongClick(final int position, String text) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                builder.setTitle("Remove allergy");
                builder.setMessage("Not alergic to " + allergies.get(position));
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        allergies.remove(position);
                        tagAllergies.removeTag(position);
                        dialogInterface.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();

            }
        });

        addAllergy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAllergicReaction();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveProfileChanges();
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDeviceSupportCamera())
                    chooseFile();
            }
        });
        findViewById(R.id.action_locate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDateOfBirth();
            }
        });
        editDob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    getDateOfBirth();
                    return true;
                }
                return false;
            }
        });
    }

    private void addAllergicReaction() {
        String allergy = editAllergy.getText().toString();
        if (TextUtils.isEmpty(allergy)) {
            layoutAllergy.setError("What are you allergic to?");
            return;
        }
        allergies.add(allergy);
        tagAllergies.setTags(allergies);
        editAllergy.setText("");
    }

    private void getDateOfBirth() {
        final SimpleDateFormat d1 = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        Calendar now = Calendar.getInstance();

        DatePickerDialog dpd = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String d = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth + " 00:00:00";
                try {
                    Date p = (new MyApplication()).formatter().parse(d);
                    CharSequence cs = DateUtils.getRelativeTimeSpanString(p.getTime(), (new Date()).getTime(), DateUtils.DAY_IN_MILLIS);
                    editDob.setText(cs);
                    date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DATE));

        dpd.getDatePicker().setMaxDate(now.getTime().getTime());
        dpd.show();

    }

    private boolean isDeviceSupportCamera() {
        // this device has a camera
// no camera on this device
        return RegisterActivity.this.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA);
    }

    private void chooseFile() {
        /*
        File uri*/
        fileUri = getOutputMediaFileUri();

        /**
         * Camera intent
         */
        final List<Intent> cameraIntents = new ArrayList<Intent>();

        Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        camera.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        cameraIntents.add(camera);

        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Intent chooserIntent = Intent.createChooser(intent, "Select a picture");

        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        startActivityForResult(chooserIntent, IMAGE_REQUEST_CODE);
    }

    private Uri getOutputMediaFileUri() {
        File file = getOutputMediaFile();
        if (file != null)
            return Uri.fromFile(file);
        return null;
    }

    private File getOutputMediaFile() {
        int permission = ActivityCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegisterActivity.this, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
            return null;
        }
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                String destPath = Environment.getExternalStorageDirectory() + File.separator + "SecurityShop" + File.separator + ".temp" + File.separator;
                mediaStorageDir = new File(destPath);
                if (!mediaStorageDir.exists())
                    if (!mediaStorageDir.mkdirs())
                        return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    private void checkPermissions() {
        List<String> ask = new ArrayList<>();
        for (String permission : PERMISSIONS_STORAGE) {
            if (!hasPermission(permission)) {
                ask.add(permission);
            }
        }
        askPermissions(ask);
    }

    private boolean hasPermission(String permission) {
        return (ActivityCompat.checkSelfPermission(RegisterActivity.this, permission) == PackageManager.PERMISSION_GRANTED);
    }

    private void askPermissions(List<String> ask) {
        if (ask != null && ask.size() > 0) {
            String[] strs = new String[ask.size()];
            for (int i = 0; i < ask.size(); i++) {
                strs[i] = ask.get(i);
            }
            ActivityCompat.requestPermissions(RegisterActivity.this, strs, REQUEST_EXTERNAL_STORAGE);
        }

    }

    private void saveProfileChanges() {
        final String name = textName.getText().toString();
        final String email = textEmail.getText().toString();
        final String phone = textPhone.getText().toString();
        final String address = textAddress.getText().toString();
        final String shopId = textShopId.getText().toString();
        final String birthday = editDob.getText().toString();
        final String password = editPassword.getText().toString();

        int gender = AppConfig.GENDER_UNSPECIFIED;
        if (radioFemale.isChecked())
            gender = AppConfig.GENDER_FEMALE;
        if (radioMale.isChecked())
            gender = AppConfig.GENDER_MALE;
        final String medicalAidName = editMedicalAidName.getText().toString();
        final String medicalAidNumber = editMedicalAidNumber.getText().toString();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < allergies.size(); i++) {
            if (i < allergies.size() - 1) {
                builder.append(allergies.get(i) + "#");
            } else {
                builder.append(allergies.get(i));
            }
        }
        final String ails = builder.toString();

        int errors = 0;
        if (TextUtils.isEmpty(name)) {
            errors++;
            layoutName.setError("Enter full name!");
        }
        if (TextUtils.isEmpty(email)) {
            errors++;
            layoutEmail.setError("Enter email address!");
        } else if (!isValidEmail(email)) {
            errors++;
            layoutEmail.setError("Enter valid email address!");
        }
        if (TextUtils.isEmpty(phone)) {
            errors++;
            layoutPhone.setError("Enter mobile number!");
        } else if (!isValidPhone(phone)) {
            errors++;
            layoutPhone.setError("Enter a valid mobile number!");
        }
        if (TextUtils.isEmpty(address)) {
            errors++;
            layoutEmail.setError("Enter your address!");
        }
        if (TextUtils.isEmpty(shopId)) {
            errors++;
            layoutShopId.setError("Enter your address!");
        }
        if (gender == AppConfig.GENDER_UNSPECIFIED) {
            errors++;
            Toast.makeText(RegisterActivity.this, "Please specify gender", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(password)) {
            errors++;
            layoutPassword.setError("Please enter password");
        }
        if (password.length() < 6) {
            errors++;
            layoutPassword.setError("Password too short");
        }
        if (profile == null) {
            String confirm = editConfirmPassword.getText().toString();
            if (!confirm.equals(password)) {
                errors++;
                layoutConfirmPassword.setError("Password mismatch");
            }
        }

        if (errors > 0)
            return;

        DatabaseReference pinRef = database.getReference(AppConfig.REF_USER_AUTH);

        Query query = pinRef.orderByKey();
        final int finalGender = gender;
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                    UserAuth auth = null;
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        UserAuth a = child.getValue(UserAuth.class);

                        if (a.getPin().equals(shopId)) {
                            auth = a;
                            break;
                        }
                    }

                    if (auth != null) {
                        pref.savePin(shopId);
                        final DatabaseReference reference = database.getReference(AppConfig.REF_USERS);
                        Query exists = reference.orderByChild("email").equalTo(email);
                        final UserAuth finalAuth = auth;
                        exists.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                                        Profile p = child.getValue(Profile.class);
                                        if (p.getEmail().equals(email)) {
                                            AlertDialog dialog = new AlertDialog.Builder(RegisterActivity.this)
                                                    .setTitle("Email address taken")
                                                    .setMessage("The email address: " + email + " is already take. If this is yours, try logging in or use a different email address!")
                                                    .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                                            finish();
                                                        }
                                                    })
                                                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            dialogInterface.dismiss();
                                                        }
                                                    })
                                                    .create();
                                            dialog.show();
                                            return;
                                        } else {
                                            final String key;
                                            if (profile != null)
                                                key = profile.getUid();
                                            else
                                                key = reference.push().getKey();

                                            url = "";
                                            profile = new Profile(key, name, finalGender, email, phone, password, address, ails, medicalAidName, medicalAidNumber, shopId, url, birthday, finalAuth.getUid());
                                            saveProfile(profile);
                                        }
                                    }

                                } else {
                                    final String key;
                                    if (profile != null)
                                        key = profile.getUid();
                                    else
                                        key = reference.push().getKey();

                                    url = "";
                                    profile = new Profile(key, name, finalGender, email, phone, password, address, ails, medicalAidName, medicalAidNumber, shopId, url, birthday, finalAuth.getUid());
                                    saveProfile(profile);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    } else {
                        showNotFoundAlert();
                    }
                } else {
                    showNotFoundAlert();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void saveProfile(final Profile profile) {

        final DatabaseReference reference = database.getReference(AppConfig.REF_USERS);

        Map<String, Object> userValues = profile.toMap();

        Map<String, Object> childValues = new HashMap<>();
        childValues.put("/" + profile.getUid(), userValues);

        reference.updateChildren(childValues);

        if (fileUri != null) {
            StorageReference store = storage.getReferenceFromUrl("gs://security-shop.appspot.com/");
            StorageReference gs = store.child("gs");
            StorageReference images = gs.child("images/" + profile.getUid());

            File file = new File(fileUri.getPath());
            if (file != null && file.isFile()) {
                String fileName = fileUri.getLastPathSegment();

                StorageReference spaceRef = images.child(fileName);

                Uri uri = Uri.fromFile(file);
                UploadTask uploadTask = spaceRef.putFile(uri);

                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        //String text = taskSnapshot.getMetadata().getDownloadUrl().getPath();                                      line commented out
                        //String image1 = taskSnapshot.getDownloadUrl().toString();
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();                                                          line commented out
                        Task<Uri> downloadUrl = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                        url = downloadUrl.toString();
                        Log.e("Firebase", downloadUrl + "\r\n");
                        reference.child(profile.getUid()).child("image").setValue(url);
                    }
                });
            }
        }
        performPostRegistrationActivity(profile);
    }

    private void showNotFoundAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
        builder.setTitle("Invalid PIN");
        builder.setMessage("The PIN number you entered did not match any PJM PIN numbers.\r\n\r\nPlease check your pin and try again or contact Security shop for help");
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void performPostRegistrationActivity(Profile profile) {
        pref.setUserUID(profile.getUid());
        pref.saveCurrentUser(profile);
        pref.savePin(profile.getSecurityShopId());
        String pin = pref.getPin();
        int type = AppConfig.getUserType(pin);
        Intent intent;
        if (type == AppConfig.USER_ADMIN) {
            intent = new Intent(RegisterActivity.this, AdminActivity.class);
        } else if (type == AppConfig.USER_AGENT) {
            intent = new Intent(RegisterActivity.this, EmergencyActivity.class);
            intent.putExtra("start", type);
        } else if (type == AppConfig.USER_MEDICAL) {
            intent = new Intent(RegisterActivity.this, EmergencyActivity.class);
            intent.putExtra("start", type);
        } else {
            intent = new Intent(RegisterActivity.this, MainActivity.class);
        }
        intent.putExtra("profile", profile);
        startActivity(intent);
        finish();
    }

    private boolean isValidPhone(String phone) {
        return !TextUtils.isEmpty(phone) && Patterns.PHONE.matcher(phone).matches();
    }

    private boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void getCurrentUser() {
        profile = pref.getCurrentUser();
        setValues();
        String user = pref.getUserUID();
        if (user != null) {
            DatabaseReference reference = database.getReference(AppConfig.REF_USERS);
            Query query = reference.orderByChild("uid").equalTo(user);
            query.keepSynced(true);

            query.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    profile = dataSnapshot.getValue(Profile.class);
                    pref.setUserUID(profile.getUid());
                    pref.saveCurrentUser(profile);
                    setValues();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    profile = dataSnapshot.getValue(Profile.class);
                    pref.setUserUID(profile.getUid());
                    pref.saveCurrentUser(profile);
                    setValues();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    pref.deleteUser();
                    profile = null;
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void setValues() {
        if (profile != null) {
            textName.setText(profile.getName());
            textEmail.setText(profile.getEmail());
            textEmail.setEnabled(false);
            textAddress.setText(profile.getAddress());
            textPhone.setText(profile.getPhone());
            textShopId.setText(pref.getPin());
            textShopId.setEnabled(false);
            editDob.setText(profile.getBirthday());
            editPassword.setText(profile.getPassword());
            layoutConfirmPassword.setVisibility(View.GONE);

            String x = profile.getAllergies();
            if (!TextUtils.isEmpty(x)) {
                String[] y = x.split("#");
                tagAllergies.setTags(y);
                allergies = new ArrayList<>();
                for (String str : y) {
                    allergies.add(str);
                }
            }
            editMedicalAidNumber.setText(profile.getMedicalAidNumber());
            editMedicalAidName.setText(profile.getMedicalAidName());

            radioMale.setChecked(profile.getGender() == AppConfig.GENDER_MALE);
            radioFemale.setChecked(profile.getGender() == AppConfig.GENDER_FEMALE);

            String url = profile.getImage();
            if (!TextUtils.isEmpty(url)) {
                try {
                    Glide.with(RegisterActivity.this)
                            .load(url)
                            .asBitmap()
                            .thumbnail(0.7f)
                            .placeholder(R.drawable.default_contact)
                            .error(R.drawable.default_contact)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new RequestListener<String, Bitmap>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                    e.printStackTrace();
                                    Log.d("Glide", "OnException invoked: " + e.getMessage());
                                    return true;
                                }

                                @Override
                                public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    Log.d("Glide", "OnResourceReady invoked");
                                    return false;
                                }
                            }).into(new ViewTarget<CircleImageView, Bitmap>(imgProfile) {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            CircleImageView img = this.view;
                            img.setImageBitmap(resource);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setupViews() {
        layoutName = (TextInputLayout) findViewById(R.id.layoutName);
        layoutEmail = (TextInputLayout) findViewById(R.id.layoutEmail);
        layoutPhone = (TextInputLayout) findViewById(R.id.layoutPhone);
        layoutAddress = (TextInputLayout) findViewById(R.id.layoutAddress);
        layoutShopId = (TextInputLayout) findViewById(R.id.layoutShopId);
        layoutDob = (TextInputLayout) findViewById(R.id.layoutDob);
        layoutMedicalAidName = (TextInputLayout) findViewById(R.id.layoutMedicalAidName);
        layoutMedicalAidNumber = (TextInputLayout) findViewById(R.id.layoutMedicalAidNumber);
        layoutAllergy = (TextInputLayout) findViewById(R.id.layoutAllergy);
        layoutPassword = (TextInputLayout) findViewById(R.id.layoutPassword);
        layoutConfirmPassword = (TextInputLayout) findViewById(R.id.layoutConfirmPassword);

        textName = (TextInputEditText) findViewById(R.id.edit_name);
        textEmail = (TextInputEditText) findViewById(R.id.edit_email);
        textPhone = (TextInputEditText) findViewById(R.id.edit_phone);
        textAddress = (TextInputEditText) findViewById(R.id.edit_address);
        textShopId = (TextInputEditText) findViewById(R.id.edit_shop_id);
        editDob = (TextInputEditText) findViewById(R.id.edit_dob);
        editMedicalAidName = (TextInputEditText) findViewById(R.id.edit_medical_aid_name);
        editMedicalAidNumber = (TextInputEditText) findViewById(R.id.edit_medical_aid_number);
        editAllergy = (TextInputEditText) findViewById(R.id.editAllergy);
        editPassword = (TextInputEditText) findViewById(R.id.editPassword);
        editConfirmPassword = (TextInputEditText) findViewById(R.id.editConfirmPassword);

        radioMale = (RadioButton) findViewById(R.id.genderMale);
        radioFemale = (RadioButton) findViewById(R.id.genderFemale);

        tagAllergies = (TagContainerLayout) findViewById(R.id.allergies);

        btnUpload = (Button) findViewById(R.id.btnUpload);
        addAllergy = (Button) findViewById(R.id.addAllergy);
        imgProfile = (CircleImageView) findViewById(R.id.imgProfile);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_REQUEST_CODE) {
            final boolean isCamera;
            if (data == null) {
                isCamera = true;
            } else {
                final String action = data.getAction();
                if (action == null) {
                    isCamera = false;
                } else {
                    isCamera = action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
                }
            }
            if (!isCamera) {
                Uri selectedImage = data == null ? null : data.getData();
                if (selectedImage != null) {
                    try {

                        String[] filePathColumn = {MediaStore.Images.Media.DATA};

                        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();

                        fileUri = Uri.parse(picturePath);
                        previewCapturedImage();

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                previewCapturedImage();
            }
        }
        if (requestCode == ALLERGIES_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    allergies = data.getStringArrayListExtra("allergies");
                    allergies.add("Add Allergy");
                    tagAllergies.setTags(allergies);
                }
            }
        }
    }

    private void previewCapturedImage() {
        if (fileUri.getPath().isEmpty()) {
            Snackbar.make(imgProfile, "Something went wrong, Please retry sending your media file", Snackbar.LENGTH_LONG).show();
            return;
        }
        File file = new File(fileUri.getPath());
        if (file != null && file.isFile()) {
            Bitmap bitmap = Util.decodeBitmap(file);
            if (bitmap != null) {
                imgProfile.setImageBitmap(bitmap);
            }
        }
    }
}
