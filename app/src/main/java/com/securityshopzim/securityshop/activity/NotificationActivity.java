package com.securityshopzim.securityshop.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.adapter.NotificationAdapter;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.objekt.NotificationItem;
import com.securityshopzim.securityshop.util.Util;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {
    SpinKitView spinKitView;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    List<NotificationAdapter.Item> data;
    NotificationAdapter adapter;

    FirebaseDatabase database;

    boolean visible = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        database = Util.getDatabase();

        spinKitView = (SpinKitView) findViewById(R.id.spinKitView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setVisibility(View.GONE);
        spinKitView.setVisibility(View.VISIBLE);
        visible = false;

        data = new ArrayList<>();
        firebasegetData();

        adapter = new NotificationAdapter(this, data);
        recyclerView.setAdapter(adapter);
    }

    private void firebasegetData() {
        DatabaseReference ref = database.getReference(AppConfig.REF_NOTIFICATION);

        Query query = ref.orderByKey();
        query.keepSynced(true);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!visible) {
                    recyclerView.setVisibility(View.VISIBLE);
                    spinKitView.setVisibility(View.GONE);
                    visible = true;
                }
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    NotificationItem item = child.getValue(NotificationItem.class);
                    adapter.addItem(item);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
