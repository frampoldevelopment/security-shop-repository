package com.securityshopzim.securityshop.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.objekt.EmergencyTip;

public class TipActivity extends AppCompatActivity {

    SpinKitView spinKitView;
    ImageView imageView;
    EmergencyTip tip;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tip);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageView = (ImageView) findViewById(R.id.tip_image);
        webView = (WebView) findViewById(R.id.webView);
        spinKitView = (SpinKitView) findViewById(R.id.spinKitView);

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            tip = (EmergencyTip) extra.getSerializable("tip");
        }

        if (tip != null) {
            try {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
                } else {
                    webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                }

                webView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        spinKitView.setVisibility(View.GONE);
                    }
                });
                webView.loadUrl(tip.getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
