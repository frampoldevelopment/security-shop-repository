package com.securityshopzim.securityshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.activity.TipActivity;
import com.securityshopzim.securityshop.adapter.viewholder.EmergencyTipViewHolder;
import com.securityshopzim.securityshop.objekt.EmergencyTip;

import java.util.List;

/**
 * Created by Framdev1 on 02/11/2016.
 */
public class EmergencyTipAdapter extends RecyclerView.Adapter<EmergencyTipViewHolder> {
    List<EmergencyTip> data;
    Context context;

    public EmergencyTipAdapter(Context context, List<EmergencyTip> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public EmergencyTipViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_tips_item, parent, false);

        return new EmergencyTipViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EmergencyTipViewHolder holder, int position) {
        final EmergencyTip tip = data.get(position);

        holder.titleView.setText(tip.getTitle());
        holder.descriptionView.setText(tip.getDescription());

        Glide.with(context)
                .load(tip.getIcon())
                .thumbnail(0.5f)
                .crossFade(1000)
                .placeholder(R.drawable.app_launcher)
                .centerCrop()
                .error(R.drawable.app_launcher)
                .skipMemoryCache(true)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        e.printStackTrace();
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TipActivity.class);
                intent.putExtra("tip", tip);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addItem(EmergencyTip tip) {
        int index = getItemExists(tip);
        if (index == -1) {
            data.add(tip);
            notifyItemInserted(data.size() - 1);
        } else {
            data.set(index, tip);
            notifyItemChanged(index);
        }
    }

    private int getItemExists(EmergencyTip tip) {
        for (int i = 0; i < data.size(); i++) {
            EmergencyTip t = data.get(i);
            if (t.getUid().equals(tip.getUid())) {
                return i;
            }
        }
        return -1;
    }
}
