package com.securityshopzim.securityshop.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.securityshopzim.securityshop.R;

/**
 * Created by Framdev1 on 02/11/2016.
 */

public class NotificationViewHolder extends RecyclerView.ViewHolder {
    public View view;
    public ImageView more;
    public TextView textTitle, textDescription, textContent;

    public NotificationViewHolder(View view) {
        super(view);
        this.view = view;
        this.textContent = (TextView) view.findViewById(R.id.notification_content);
        this.textDescription = (TextView) view.findViewById(R.id.descriptionView);
        this.textTitle = (TextView) view.findViewById(R.id.titleView);
        this.more = (ImageView) view.findViewById(R.id.show_more);
    }
}
