package com.securityshopzim.securityshop.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.securityshopzim.securityshop.R;

/**
 * Created by Framdev1 on 03/11/2016.
 */

public class EmergencyCallViewHolder extends RecyclerView.ViewHolder {
    public View view;
    public ImageView imageView;
    public TextView textName, textDetail, textLocation, firstLetter;

    public EmergencyCallViewHolder(View view) {
        super(view);
        this.view = view;
        this.imageView = (ImageView) view.findViewById(R.id.circleImageView);
        this.textName = (TextView) view.findViewById(R.id.victimName);
        this.textDetail = (TextView) view.findViewById(R.id.victimInfo);
        this.textLocation = (TextView) view.findViewById(R.id.victimLocation);
        this.firstLetter = (TextView) view.findViewById(R.id.firstLetter);
    }
}
