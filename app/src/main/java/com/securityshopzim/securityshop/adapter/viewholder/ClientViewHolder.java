package com.securityshopzim.securityshop.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.securityshopzim.securityshop.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Framdev1 on 23/11/2016.
 */

public class ClientViewHolder extends RecyclerView.ViewHolder {
    public View view;
    public TextView textInit, textName, textNumber, textPin;
    public ImageView imageDelete;
    public CircleImageView circleImageView;

    public ClientViewHolder(View view) {
        super(view);
        this.view = view;
        this.textInit = (TextView) view.findViewById(R.id.firstLetter);
        this.textName = (TextView) view.findViewById(R.id.name);
        this.textNumber = (TextView) view.findViewById(R.id.client_phone);
        this.textPin = (TextView) view.findViewById(R.id.client_number);

        this.imageDelete = (ImageView) view.findViewById(R.id.imageView);
        this.circleImageView = (CircleImageView) view.findViewById(R.id.circleImageView);
    }
}
