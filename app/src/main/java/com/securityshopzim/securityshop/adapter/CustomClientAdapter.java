package com.securityshopzim.securityshop.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.target.ViewTarget;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.adapter.viewholder.ClientViewHolder;
import com.securityshopzim.securityshop.objekt.Profile;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Framdev1 on 23/11/2016.
 */

public class CustomClientAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<Profile> data;

    public CustomClientAdapter(Context context, List<Profile> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == -100) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_client_header, parent, false);
            return new CaptionViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_client_item, parent, false);
            return new ClientViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        final Profile profile = data.get(position);

        if (profile.getGender() == -100) {
            CaptionViewHolder h = (CaptionViewHolder) viewHolder;
            h.textView.setText(profile.getName());
        } else {
            final ClientViewHolder holder = (ClientViewHolder) viewHolder;
            String name = profile.getName();
            if (name != null) {
                holder.textName.setText(name);
                char letter = name.charAt(0);
                holder.textInit.setText(String.valueOf(letter));
            }
            String phone = profile.getPhone();
            if (TextUtils.isEmpty(phone)) {
                phone = profile.getEmail();
            }
            holder.textNumber.setText(phone);
            holder.textPin.setText(profile.getSecurityShopId());

            String url = profile.getImage();
            if (!TextUtils.isEmpty(url)) {
                try {
                    Glide.with(context)
                            .load(url)
                            .asBitmap()
                            .thumbnail(0.7f)
                            .placeholder(R.drawable.default_contact)
                            .error(R.drawable.default_contact)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new RequestListener<String, Bitmap>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                    e.printStackTrace();
                                    Log.d("Glide", "OnException invoked: " + e.getMessage());
                                    return true;
                                }

                                @Override
                                public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    Log.d("Glide", "OnResourceReady invoked");
                                    return false;
                                }
                            }).into(new ViewTarget<CircleImageView, Bitmap>(holder.circleImageView) {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            CircleImageView img = this.view;
                            img.setImageBitmap(resource);
                            holder.textInit.setVisibility(View.GONE);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String message = "Editing this client profile";
                    if (profile.getBirthday().equals("agent")) {
                        message = "Editing this agent profile";
                    }
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            });
            holder.imageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String message = "Are you sure you want to remove CLIENT " + profile.getName() + ".\r\n\r\nThis client will no longer have access to PJM Response";
                    if (profile.getBirthday().equals("agent")) {
                        message = "Are you sure you want to remove AGENT " + profile.getName() + ".\r\n\r\nThis client will no longer have access to PJM Response";
                    }
                    AlertDialog dialog = new AlertDialog.Builder(context)
                            .setTitle("Remove client?")
                            .setMessage(message)
                            .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Toast.makeText(context, "Removing client", Toast.LENGTH_LONG).show();
                                    dialogInterface.dismiss();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create();
                    dialog.show();
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        Profile profile = data.get(position);
        return profile.getGender();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addItem(Profile profile) {
        int index = profileExists(profile);
        if (index == -1) {
            data.add(profile);
            notifyItemInserted(data.size() - 1);
        } else {
            data.add(index, profile);
            notifyItemChanged(index);
        }
    }

    public int profileExists(Profile profile) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getUid().equals(profile.getUid()))
                return i;
        }
        return -1;
    }

    class CaptionViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public TextView textView;

        public CaptionViewHolder(View view) {
            super(view);
            this.view = view;
            this.textView = (TextView) view.findViewById(R.id.caption);
        }
    }
}
