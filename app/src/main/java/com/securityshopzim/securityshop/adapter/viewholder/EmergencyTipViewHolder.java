package com.securityshopzim.securityshop.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.securityshopzim.securityshop.R;

/**
 * Created by Framdev1 on 02/11/2016.
 */
public class EmergencyTipViewHolder extends RecyclerView.ViewHolder {
    public View view;
    public ImageView imageView;
    public TextView titleView, descriptionView;

    public EmergencyTipViewHolder(View view) {
        super(view);
        this.view = view;
        this.imageView = (ImageView) view.findViewById(R.id.imageView);
        this.titleView = (TextView) view.findViewById(R.id.titleView);
        this.descriptionView = (TextView) view.findViewById(R.id.descriptionView);
    }
}
