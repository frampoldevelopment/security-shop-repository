package com.securityshopzim.securityshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.activity.MapsActivity;
import com.securityshopzim.securityshop.activity.singleUserMapsActivity;
import com.securityshopzim.securityshop.adapter.viewholder.EmergencyCallViewHolder;
import com.securityshopzim.securityshop.app.MyApplication;
import com.securityshopzim.securityshop.fragment.EmergencyFragment;
import com.securityshopzim.securityshop.objekt.EmergencyCall;
import com.securityshopzim.securityshop.objekt.Wrapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Framdev1 on 03/11/2016.
 */

public class EmergencyCallAdapter extends RecyclerView.Adapter<EmergencyCallViewHolder> {
    Context context;
    List<EmergencyCall> data;

    public EmergencyCallAdapter(Context context, List<EmergencyCall> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public EmergencyCallViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0)
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_emergency_call_unread, parent, false);
        else
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_emergency_call, parent, false);
        return new EmergencyCallViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EmergencyCallViewHolder holder, int position) {
        final EmergencyCall call = data.get(position);
        String caller = call.getCaller();
        if (caller != null) {
            holder.textName.setText(caller);
            char letter = caller.charAt(0);
            holder.firstLetter.setText(String.valueOf(letter));
        }
        holder.textDetail.setText(call.getPhone());
        try {
            Date p = (new MyApplication()).formatter().parse(call.getDate());
            CharSequence cs = DateUtils.getRelativeTimeSpanString(p.getTime(), (new Date()).getTime(), DateUtils.DAY_IN_MILLIS);
            holder.textLocation.setText(cs);

        } catch (ParseException e) {
            e.printStackTrace();
            holder.textLocation.setText(call.getLocation());
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, singleUserMapsActivity.class);
                List<EmergencyCall> calls = new ArrayList<>();
                calls.add(call);
                Wrapper wrapper = new Wrapper(calls);
                intent.putExtra("wrapper", wrapper);
                context.startActivity(intent);

                EmergencyFragment.callIsRead(call, 1);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getRead();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addEmergencyItem(EmergencyCall item) {
        int index = itemExists(item);
        if (index == -1) {
            data.add(0, item);
            notifyItemInserted(0);
        } else {
            data.add(index, item);
            notifyItemChanged(index);
        }
    }

    private int itemExists(EmergencyCall item) {
        for (int i = 0; i < data.size(); i++) {
            EmergencyCall call = data.get(i);
            if (call.getUid().equals(item.getUid()))
                return i;
        }
        return -1;
    }

    public void changeItem(EmergencyCall item) {
        int index = itemExists(item);
        try {
            if (index != -1) {
                data.add(index, item);
                notifyItemInserted(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
