package com.securityshopzim.securityshop.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.adapter.viewholder.NotificationViewHolder;
import com.securityshopzim.securityshop.objekt.NotificationItem;

import java.util.List;

/**
 * Created by Framdev1 on 02/11/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationViewHolder> {
    Context context;
    List<Item> data;

    public NotificationAdapter(Context context, List<Item> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custion_notification_item, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NotificationViewHolder holder, final int position) {
        final Item i = data.get(position);
        final NotificationItem item = i.item;
        final boolean expanded = i.expanded;
        holder.textTitle.setText(item.getTitle());
        holder.textDescription.setText(item.getDescription());
        holder.textContent.setText(item.getContent());

        if (expanded) {
            holder.textContent.setVisibility(View.VISIBLE);
        } else {
            holder.textContent.setVisibility(View.GONE);
        }
        if (item.getRead() == 0) {
            holder.textTitle.setTypeface(null, Typeface.BOLD);
            holder.textDescription.setTypeface(null, Typeface.BOLD);
            holder.more.setColorFilter(ContextCompat.getColor(context, android.R.color.black));
        } else {
            holder.textTitle.setTypeface(null, Typeface.NORMAL);
            holder.textDescription.setTypeface(null, Typeface.NORMAL);
            holder.more.setColorFilter(ContextCompat.getColor(context, android.R.color.darker_gray));
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item.setRead(1);
                i.item = item;
                if (expanded) {
                    holder.textContent.setVisibility(View.GONE);
                    i.expanded = false;
                } else {
                    holder.textContent.setVisibility(View.VISIBLE);
                    i.expanded = true;
                }
                data.set(position, i);
                notifyItemChanged(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addItem(NotificationItem item) {
        int index = itemExists(item);
        if (index == -1) {
            data.add(new Item(item, false));
            notifyItemInserted(data.size() - 1);
        } else {
            data.add(index, new Item(item, false));
            notifyItemChanged(index);
        }
    }

    private int itemExists(NotificationItem item) {
        for (int i = 0; i < data.size(); i++) {
            NotificationItem x = data.get(i).item;
            if (x.getUid().equals(item.getUid()))
                return i;
        }
        return -1;
    }

    public static class Item {
        public NotificationItem item;
        public boolean expanded;

        public Item(NotificationItem item, boolean expanded) {
            this.item = item;
            this.expanded = expanded;
        }
    }
}
