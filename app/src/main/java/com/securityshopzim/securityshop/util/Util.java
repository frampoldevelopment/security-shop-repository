package com.securityshopzim.securityshop.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.IOException;

/**
 * Created by Framdev1 on 03/11/2016.
 */

public class Util {
    private static FirebaseDatabase database;

    public static FirebaseDatabase getDatabase() {
        try {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        } catch (Exception e) {

        }
        if (database == null) {
            database = FirebaseDatabase.getInstance();
        }
        return database;
    }

    public static Bitmap decodeBitmap(File file) {
        if (file.isFile()) {
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
            try {
                ExifInterface exif = new ExifInterface(file.getPath());
                String location = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE) + " longitude " + exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                String date = exif.getAttribute(ExifInterface.TAG_DATETIME);
                String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
                int orientation = orientString != null
                        ? Integer.parseInt(orientString)
                        : ExifInterface.ORIENTATION_NORMAL;

                int rotationAngle = 0;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
                    rotationAngle = 90;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
                    rotationAngle = 180;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
                    rotationAngle = 270;

                Matrix matrix = new Matrix();
                matrix.setRotate(rotationAngle, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
                Bitmap rotatedImage = Bitmap.createBitmap(bitmap, 0, 0, options.outWidth, options.outHeight, matrix, true);
                return rotatedImage;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return bitmap;
        }
        return null;
    }
}
