package com.securityshopzim.securityshop.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.activity.MapsActivity;
import com.securityshopzim.securityshop.app.MyApplication;
import com.securityshopzim.securityshop.helper.PrefManager;

/**
 * Created by Framdev1 on 07/11/2016.
 */

public class NotificationUtil {
    public static final String TAG = NotificationUtil.class.getSimpleName();
    Context context;
    PrefManager pref;

    public NotificationUtil(Context context) {
        this.context = context;
        pref = new PrefManager(context);
    }

    public void showNotification(String title, String message, String longMessage, String uid, String timestamp) {
        if (MyApplication.isActivityVisible()) {
            playSound(1);
        } else {
            int icon = R.mipmap.ic_launcher;
            int notificationId = pref.getNotificationId();

            Intent notificationIntent = new Intent(context, MapsActivity.class);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

            builder.setSmallIcon(icon);
            builder.setTicker(title);
            builder.setContentTitle(title);
            builder.setContentText(message);
            builder.setContentIntent(pendingIntent);
            builder.setAutoCancel(true);
            builder.setPriority(Notification.PRIORITY_MAX);
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            builder.addAction(R.drawable.ic_vector_accept, "More", pendingIntent);

            NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
            bigText.bigText(longMessage);
            bigText.setSummaryText("The Security Shop");
            bigText.setBigContentTitle(title);
            builder.setStyle(bigText);

            // Create Notification Manager
            NotificationManager notificationmanager = (NotificationManager) MyApplication.getAppContext()
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            // Build Notification with Notification Manager
            Notification notification = builder.build();
            notification.flags = Notification.FLAG_INSISTENT;
            notificationmanager.notify(notificationId, notification);
        }
    }

    private void playSound(int freq) {

    }
}
