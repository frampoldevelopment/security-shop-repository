package com.securityshopzim.securityshop.objekt;

import java.util.HashMap;
import java.util.Map;

public class ClientLocation {
    private double latitude, longitude;

    public ClientLocation(){

    }

    public ClientLocation(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude () {
        return latitude;
    }

    public double getLongitude () {
        return longitude;
    }

    public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<>();

        result.put("latitude", latitude);
        result.put("longitude", longitude);
        return  result;
    }
}