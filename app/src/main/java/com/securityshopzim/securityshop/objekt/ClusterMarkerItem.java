package com.securityshopzim.securityshop.objekt;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.ui.IconGenerator;
import com.securityshopzim.securityshop.app.MyApplication;

/**
 * Created by Framdev1 on 19/09/2016.
 */
public class ClusterMarkerItem implements ClusterItem {
    String uid;
    int read;
    private LatLng mPosition;
    private String snippet;
    private String title;

    public ClusterMarkerItem(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
        snippet = "";
    }

    public ClusterMarkerItem(String uid, double lat, double lng, String snippet, String title, int read) {
        mPosition = new LatLng(lat, lng);
        this.uid = uid;
        this.snippet = snippet;
        this.title = title;
        this.read = read;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public void setPosition(LatLng mPosition) {
        this.mPosition = mPosition;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public String getUid() {
        return uid;
    }

    public BitmapDescriptor getIconDescriptor() {
        IconGenerator iconFactory = new IconGenerator(MyApplication.getAppContext());
        switch (read) {
            case 0:
                iconFactory.setStyle(IconGenerator.STYLE_RED);
                break;
            case 1:
                iconFactory.setStyle(IconGenerator.STYLE_BLUE);
                break;
            case 2:
                iconFactory.setStyle(IconGenerator.STYLE_WHITE);
                break;
            case 3:
                iconFactory.setStyle(IconGenerator.STYLE_BLUE);
                break;
            default:
                iconFactory.setStyle(IconGenerator.STYLE_ORANGE);
                break;
        }
        return BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(title));
    }

    public String getTitle() {
        return title;
    }

    public String getSnippet() {
        return snippet;
    }
}
