package com.securityshopzim.securityshop.objekt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Framdev1 on 18/11/2016.
 * for auth details
 */
public class UserAuth implements Serializable {
    private String uid;
    private String pin;
    private String password;
    private String email;

    public UserAuth() {
        //Default constructor to be used by dataSnapshot.getValue(UserAuth.class)
    }

    public UserAuth(String uid, String pin, String password, String email) {
        this.uid = uid;
        this.pin = pin;
        this.password = password;
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public String getPin() {
        return pin;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        map.put("uid", uid);
        map.put("pin", pin);
        map.put("password", password);
        map.put("email", email);

        return map;
    }
}
