package com.securityshopzim.securityshop.objekt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Framdev1 on 02/11/2016.
 */

public class EmergencyTip implements Serializable {
    private String uid;
    private String title;
    private String description;
    private String content; //HTML
    private String icon;

    public EmergencyTip() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public EmergencyTip(String uid, String title, String description, String content, String icon) {
        this.uid = uid;
        this.title = title;
        this.description = description;
        this.content = content;
        this.icon = icon;
    }

    public String getUid() {
        return uid;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getContent() {
        return content;
    }

    public String getIcon() {
        return icon;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("uid", uid);
        result.put("title", title);
        result.put("description", description);
        result.put("content", content);
        result.put("icon", icon);

        return result;
    }
}
