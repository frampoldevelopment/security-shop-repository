package com.securityshopzim.securityshop.objekt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Framdev1 on 03/11/2016.
 */

public class EmergencyCall implements Serializable {
    private String uid;
    private String date;
    private String user;
    private String caller;
    private int type;
    private String location;
    private String phone;
    private int read;

    private EmergencyCall() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public EmergencyCall(String uid, String date, String user, String caller, int type, String location, String phone, int read) {
        this.uid = uid;
        this.date = date;
        this.user = user;
        this.type = type;
        this.location = location;
        this.phone = phone;
        this.caller = caller;
        this.read = read;
    }

    public String getUid() {
        return uid;
    }

    public String getDate() {
        return date;
    }

    public String getUser() {
        return user;
    }

    public String getCaller() {
        return caller;
    }

    public int getType() {
        return type;
    }

    public String getLocation() {
        return location;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public String getPhone() {
        return phone;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("uid", uid);
        result.put("date", date);
        result.put("user", user);
        result.put("caller", caller);
        result.put("type", type);
        result.put("location", location);
        result.put("phone", phone);
        result.put("read", read);

        return result;
    }
}