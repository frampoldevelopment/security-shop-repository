package com.securityshopzim.securityshop.objekt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Framdev1 on 02/11/2016.
 */
public class NotificationItem implements Serializable {
    private String uid;
    private String title;
    private String description;
    private String content;
    private int read;
    private int urgency;

    public NotificationItem() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public NotificationItem(String uid, String title, String description, String content, int read, int urgency) {
        this.uid = uid;
        this.title = title;
        this.description = description;
        this.content = content;
        this.read = read;
        this.urgency = urgency;
    }

    public String getUid() {
        return uid;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getContent() {
        return content;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public int getUrgency() {
        return urgency;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("uid", uid);
        result.put("title", title);
        result.put("description", description);
        result.put("content", content);
        result.put("read", read);
        result.put("urgency", urgency);

        return result;
    }
}
