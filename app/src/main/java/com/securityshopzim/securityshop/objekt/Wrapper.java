package com.securityshopzim.securityshop.objekt;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Framdev1 on 22/11/2016.
 */

public class Wrapper implements Serializable {
    int count = 0;
    List<EmergencyCall> calls;

    public Wrapper(List<EmergencyCall> calls) {
        this.calls = calls;
        this.count = calls.size();
    }

    public int getCount() {
        return count;
    }

    public List<EmergencyCall> getCalls() {
        return calls;
    }
}
