package com.securityshopzim.securityshop.objekt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Framdev1 on 23/11/2016.
 */

public class ResponseAgent implements Serializable {
    private String uid;
    private String name;
    private String post;
    private int type;
    private String email;
    private String phone;

    public ResponseAgent() {
    }

    public ResponseAgent(String uid, String name, String post, int type, String email, String phone) {
        this.uid = uid;
        this.name = name;
        this.post = post;
        this.type = type;
        this.email = email;
        this.phone = phone;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        map.put("uid", uid);
        map.put("name", name);
        map.put("email", email);
        map.put("phone", phone);
        map.put("post", post);
        map.put("type", type);

        return map;
    }

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getPost() {
        return post;
    }

    public int getType() {
        return type;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
