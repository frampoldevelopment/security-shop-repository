package com.securityshopzim.securityshop.objekt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Framdev1 on 03/11/2016.
 */

public class Profile implements Serializable {
    private String auth;
    private String uid;
    private String name;
    private String email;
    private String phone;
    private String address;
    private String securityShopId;
    private String birthday;
    private String image;
    private String allergies;
    private String medicalAidName;
    private String medicalAidNumber;
    private int gender;
    private String password;

    public Profile() {
    }

    public Profile(String uid, String name, int gender, String email, String phone, String password, String address, String allergies, String medicalAidName, String medicalAidNumber, String securityShopId, String image, String birthday, String auth) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.securityShopId = securityShopId;
        this.birthday = birthday;
        this.image = image;
        this.gender = gender;
        this.medicalAidName = medicalAidName;
        this.medicalAidNumber = medicalAidNumber;
        this.allergies = allergies;
        this.auth = auth;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getAuth() {
        return auth;
    }

    public String getUid() {
        return uid;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getSecurityShopId() {
        return securityShopId;
    }

    public String getBirthday() {
        return birthday;
    }

    public int getGender() {
        return gender;
    }

    public String getAllergies() {
        return allergies;
    }

    public String getMedicalAidName() {
        return medicalAidName;
    }

    public String getMedicalAidNumber() {
        return medicalAidNumber;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> result = new HashMap<>();

        result.put("uid", uid);
        result.put("name", name);
        result.put("email", email);
        result.put("phone", phone);
        result.put("address", address);
        result.put("birthday", birthday);
        result.put("securityShopId", securityShopId);
        result.put("image", image);
        result.put("gender", gender);
        result.put("medicalAidName", medicalAidName);
        result.put("medicalAidNumber", medicalAidNumber);
        result.put("allergies", allergies);
        result.put("auth", auth);
        result.put("password", password);

        return result;
    }
}