package com.securityshopzim.securityshop.helper;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.app.MyApplication;
import com.securityshopzim.securityshop.objekt.EmergencyCall;
import com.securityshopzim.securityshop.util.NotificationUtil;
import com.securityshopzim.securityshop.util.Util;

/**
 * Created by Framdev1 on 07/11/2016.
 */

public class EmergencyService extends Service {
    NotificationUtil util;
    FirebaseDatabase database;
    PrefManager pref;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "Starting to monitor emergencies");
        pref = new PrefManager(MyApplication.getAppContext());
        listenToEmergencies();
        return super.onStartCommand(intent, flags, startId);
    }

    private void listenToEmergencies() {
        Log.d("Service", "Service is checking emergencies");
        database = Util.getDatabase();

        DatabaseReference reference = database.getReference(AppConfig.REF_EMERGENCY);

        Query query = reference.orderByKey();
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("Service", "Service found something");
                EmergencyCall call = dataSnapshot.getValue(EmergencyCall.class);
                if (pref.isEmergencyNotifiable(call.getUid())) {
                    Log.d("Service", "Service Posting notification");
                    util = new NotificationUtil(MyApplication.getAppContext());
                    util.showNotification(call.getCaller(), call.getLocation(), call.getLocation(), call.getUid(), call.getDate());
                    pref.addEmergency(call.getUid());
                } else {
                    Log.d("Service", "Service will not show notification");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
