package com.securityshopzim.securityshop.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.objekt.Profile;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Framdev1 on 03/11/2016.
 */

public class PrefManager {
    public static final String CURRENT_USER = "current_uid";
    private static final String IS_FIRST_TIME_LAUNCH = "first_launch";
    private static final String USER_UID = "user_uid";
    private static final String USER_NAME = "user_name";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_PHONE = "user_phone";
    private static final String USER_ADDRESS = "user_address";
    private static final String USER_SSID = "user_ssid";
    private static final String USER_IMAGE = "user_image_url";
    private static final String USER_BDAY = "user_bday";
    private static final String NOTIFICATION_ID = "notification_id";
    private static final String SET_EMERGENCIES = "set_of_emergencies";
    private static final String USER_MEDICAL_NAME = "medical_aid_name";
    private static final String USER_MEDICAL_NUMBER = "medical_aid_number";
    private static final String USER_GENDER = "user_gender";
    private static final String USER_ALLERGIES = "user_allergies";
    private static final String USER_AUTH = "user_auth";
    private static final String USER_PASSWORD = "user_password";
    // Shared pref file name
    private static final String PREF_NAME = "FanuelMpanje";
    private static final String KEY_PIN = "shop_pin";
    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public String getUserUID() {
        return pref.getString(CURRENT_USER, "");
    }

    public void setUserUID(String uid) {
        editor.putString(CURRENT_USER, uid).commit();
    }

    public void saveCurrentUser(Profile profile) {
        editor.putString(USER_UID, profile.getUid());
        editor.putString(USER_NAME, profile.getName());
        editor.putString(USER_EMAIL, profile.getEmail());
        editor.putString(USER_PHONE, profile.getPhone());
        editor.putString(USER_ADDRESS, profile.getAddress());
        editor.putString(USER_SSID, profile.getSecurityShopId());
        editor.putString(USER_IMAGE, profile.getImage());
        editor.putString(USER_BDAY, profile.getBirthday());
        editor.putString(USER_MEDICAL_NAME, profile.getMedicalAidName());
        editor.putString(USER_MEDICAL_NUMBER, profile.getMedicalAidNumber());
        editor.putInt(USER_GENDER, profile.getGender());
        editor.putString(USER_ALLERGIES, profile.getAllergies());
        editor.putString(USER_AUTH, profile.getAuth());
        editor.putString(USER_PASSWORD, profile.getPassword());
        editor.commit();
    }

    public Profile getCurrentUser() {
        String uid = pref.getString(USER_UID, null);
        if (TextUtils.isEmpty(uid))
            return null;
        String name = pref.getString(USER_NAME, "");
        String email = pref.getString(USER_EMAIL, "");
        String phone = pref.getString(USER_PHONE, "");
        String address = pref.getString(USER_ADDRESS, "");
        String ssid = pref.getString(USER_SSID, "");
        String image = pref.getString(USER_IMAGE, "");
        String bday = pref.getString(USER_BDAY, "");
        int gender = pref.getInt(USER_GENDER, AppConfig.GENDER_UNSPECIFIED);
        String allergies = pref.getString(USER_ALLERGIES, "");
        String maName = pref.getString(USER_MEDICAL_NAME, "");
        String maNumber = pref.getString(USER_MEDICAL_NUMBER, "");
        String auth = pref.getString(USER_AUTH, "");
        String password = pref.getString(USER_PASSWORD, "");
        return new Profile(uid, name, gender, email, phone, password, address, allergies, maName, maNumber, ssid, image, bday, auth);
    }

    public void deleteUser() {
        editor.remove(CURRENT_USER).commit();
        editor.remove(USER_UID).commit();
    }

    public int getNotificationId() {
        int id = pref.getInt(NOTIFICATION_ID, 0);
        editor.putInt(NOTIFICATION_ID, id++);
        editor.commit();
        return id;
    }

    public void addEmergency(String uid) {
        Set<String> calls = pref.getStringSet(SET_EMERGENCIES, new HashSet<String>());
        calls.add(uid);
        editor.putStringSet(SET_EMERGENCIES, calls).commit();
    }

    public boolean isEmergencyNotifiable(String uid) {
        Set<String> calls = pref.getStringSet(SET_EMERGENCIES, new HashSet<String>());
        for (String string : calls) {
            if (string.equals(uid))
                return false;
        }
        return true;
    }

    public void savePin(String pin) {
        editor.putString(KEY_PIN, pin).commit();
    }

    public String getPin() {
        return pref.getString(KEY_PIN, "");
    }
}
