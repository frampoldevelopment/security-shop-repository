package com.securityshopzim.securityshop.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Framdev1 on 04/11/2016.
 * This records a call if it has an ID of 100
 */

public class OutgoingCallInterceptor extends BroadcastReceiver {
    private static final String LOG_TAG = "OutgoingCallInterceptor";
    Bundle bundle;
    String phoneNumber;

    MediaRecorder recorder;
    boolean recordStarted;

    int shouldRecord = 0;
    MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mediaRecorder, int i, int i1) {

        }
    };
    MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mediaRecorder, int i, int i1) {

        }
    };

    public OutgoingCallInterceptor() {
        this.shouldRecord = 0;
    }

    public OutgoingCallInterceptor(int shouldRecord) {
        this.shouldRecord = shouldRecord;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (shouldRecord == 0) {
            Log.e("onReceive", "I am not going to record this call");

            return;
        }
        Log.e("onReceive", "I am now going to record this call");
        if (intent != null) {
            bundle = intent.getExtras();
            if (bundle != null) {
                phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
                Log.d(LOG_TAG, phoneNumber);
            }
        }

        try {
            if (bundle != null) {
                //String state = bundle.getString(TelephonyManager.EXTRA_STATE);

                //Log.d("STATE", state);
                TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                tm.listen(new PhoneStateListener() {

                    @Override
                    public void onCallStateChanged(int state, String incomingNumber) {
                        super.onCallStateChanged(state, incomingNumber);
                        switch (state) {
                            case TelephonyManager.CALL_STATE_RINGING:
                                Log.d(LOG_TAG, "RINGING");
                                Log.d(LOG_TAG, "Phone is now ringing");
                                break;
                            case TelephonyManager.CALL_STATE_OFFHOOK:
                                Log.d(LOG_TAG, "OFF HOOK");
                                Log.d(LOG_TAG, "We are now in call... starting to record");

                                Log.d(LOG_TAG, "Preparing Media Recorder");
                                recorder = new MediaRecorder();
                                recorder.reset();

                                Log.d(LOG_TAG, "Media recorder initialised");


                                File file = new File(Environment.getExternalStorageDirectory() + "/SecurityShop");
                                if (!file.exists()) {
                                    file.mkdirs();
                                    Log.d(LOG_TAG, "Directory made");
                                }
                                String filename = getFileName();
                                File audio;
                                try {
                                    audio = File.createTempFile(filename, ".amr", file);
                                } catch (Exception e) {
                                    audio = file;
                                    e.printStackTrace();
                                }

                                recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
                                recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
                                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                                recorder.setOutputFile(audio.getAbsolutePath());

                                Log.d(LOG_TAG, "Necessary parameters set");


                                recorder.setOnErrorListener(errorListener);
                                recorder.setOnInfoListener(infoListener);

                                Log.d(LOG_TAG, "Listeners set");

                                try {
                                    Log.d(LOG_TAG, "About to start recording");
                                    recorder.prepare();
                                    recorder.start();
                                    recordStarted = true;
                                    Log.d(LOG_TAG, "Recording started");
                                } catch (IllegalStateException | IOException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case TelephonyManager.CALL_STATE_IDLE:
                                Log.d(LOG_TAG, "IDLE");
                                Log.e(LOG_TAG, "REJECT || DISCO");
                                if (recordStarted) {
                                    try {
                                        recorder.stop();
                                        recordStarted = false;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                break;
                            default:
                                Log.d(LOG_TAG, "Default: " + state);
                                break;
                        }
                    }
                }, PhoneStateListener.LISTEN_CALL_STATE);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getFileName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        return "REC_" + timeStamp;
    }

}
