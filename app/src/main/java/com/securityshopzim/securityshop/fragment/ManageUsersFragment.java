package com.securityshopzim.securityshop.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.adapter.CustomClientAdapter;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.objekt.Profile;
import com.securityshopzim.securityshop.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Framdev1 on 23/11/2016.
 */

public class ManageUsersFragment extends DialogFragment {

    List<Profile> data;
    CustomClientAdapter adapter;
    RecyclerView recyclerView;
    SpinKitView spinKitView;

    FirebaseDatabase database;

    boolean visible = false;

    public static ManageUsersFragment newInstance() {
        return new ManageUsersFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.fragment_dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_users, container, false);
        database = Util.getDatabase();

        spinKitView = (SpinKitView) view.findViewById(R.id.spinKitView);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        data = new ArrayList<>();
        data.add(new Profile("client", "PJM Administrators", -100, "", "", "", "", "", "", "", "", "", "", ""));
        adapter = new CustomClientAdapter(getActivity(), data);
        recyclerView.setAdapter(adapter);

        recyclerView.setVisibility(View.GONE);
        spinKitView.setVisibility(View.VISIBLE);

        firebaseData();

        return view;
    }

    private void firebaseData() {
        DatabaseReference reference = database.getReference(AppConfig.REF_USERS);

        Query query = reference.orderByChild("name");
        query.keepSynced(true);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    if (!visible) {
                        spinKitView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    Profile profile = child.getValue(Profile.class);
                    int type = AppConfig.getUserType(profile.getSecurityShopId());
                    if (type == AppConfig.USER_ADMIN)
                        adapter.addItem(profile);
                }
                addAgents();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addAgents() {
        DatabaseReference reference = database.getReference(AppConfig.REF_USERS);

        Query query = reference.orderByChild("name");
        query.keepSynced(true);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0)
                    adapter.addItem(new Profile("agent", "PJM Response Agents", -100, "", "", "", "", "", "", "", "", "", "", ""));
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Profile profile = child.getValue(Profile.class);
                    int type = AppConfig.getUserType(profile.getSecurityShopId());
                    if (type == AppConfig.USER_AGENT || type == AppConfig.USER_MEDICAL)
                        adapter.addItem(profile);
                }
                addClients();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addClients() {
        DatabaseReference reference = database.getReference(AppConfig.REF_USERS);

        Query query = reference.orderByChild("name");
        query.keepSynced(true);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0)
                    adapter.addItem(new Profile("agent", "PJM Response Clients", -100, "", "", "", "", "", "", "", "", "", "", ""));
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Profile profile = child.getValue(Profile.class);
                    int type = AppConfig.getUserType(profile.getSecurityShopId());
                    if (type == AppConfig.USER_CLIENT)
                        adapter.addItem(profile);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
