package com.securityshopzim.securityshop.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.objekt.UserAuth;
import com.securityshopzim.securityshop.util.Util;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Framdev1 on 23/11/2016.
 */

public class NewUserFragment extends DialogFragment {

    FirebaseDatabase database;
    TextInputEditText editPin;
    TextInputEditText editEmail, editPhone;

    TextInputLayout layoutEmail, layoutPhone, layoutPin;

    public static NewUserFragment newInstance() {
        return new NewUserFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.fragment_dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_user, container, false);
        if (database == null)
            database = Util.getDatabase();

        editPin = (TextInputEditText) view.findViewById(R.id.edit_shop_id);
        editEmail = (TextInputEditText) view.findViewById(R.id.edit_email);
        editPhone = (TextInputEditText) view.findViewById(R.id.edit_phone);

        layoutEmail = (TextInputLayout) view.findViewById(R.id.layoutEmail);
        layoutPhone = (TextInputLayout) view.findViewById(R.id.layoutPhone);
        layoutPin = (TextInputLayout) view.findViewById(R.id.layoutShopId);


        view.findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUserAuth();
            }
        });

        return view;
    }

    private void saveUserAuth() {
        String email = editEmail.getText().toString();
        String phone = editPhone.getText().toString();
        final String pin = editPin.getText().toString();
        int res = 0;
        if (TextUtils.isEmpty(phone)) {
            layoutPhone.setError("Please enter phone number");
        }
        if (TextUtils.isEmpty(email)) {
            layoutEmail.setError("Please enter email address");
            res++;
        }
        if (TextUtils.isEmpty(pin)) {
            layoutPin.setError("Please enter PJM PIN");
            res++;
        }
        if (res > 0) {
            return;
        }
        String identifier = email;
        if (TextUtils.isEmpty(identifier))
            identifier = phone;

        final DatabaseReference ref = database.getReference(AppConfig.REF_USER_AUTH);
        Query query = ref.orderByChild("email").equalTo(email);
        final String finalIdentifier = identifier;
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    String key = ref.push().getKey();

                    UserAuth auth = new UserAuth(key, pin, "default", finalIdentifier);

                    Map<String, Object> map = auth.toMap();

                    Map<String, Object> child = new HashMap<>();
                    child.put("/" + key, map);

                    ref.updateChildren(child);

                    Toast.makeText(getActivity(), "New Client created", Toast.LENGTH_LONG).show();

                    getDialog().dismiss();
                    dismiss();
                } else {
                    Toast.makeText(getActivity(), "Email address already taken", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private String generateRandom() {
        String numbers = "0123456789";

        char[] nums = numbers.toCharArray();
        Random r = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 6; i++) {

            builder.append(nums[r.nextInt(9)]);
        }
        return builder.toString();
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
