package com.securityshopzim.securityshop.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.activity.AdminActivity;
import com.securityshopzim.securityshop.activity.EmergencyActivity;
import com.securityshopzim.securityshop.activity.MainActivity;
import com.securityshopzim.securityshop.activity.RegisterActivity;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.helper.PrefManager;
import com.securityshopzim.securityshop.objekt.UserAuth;
import com.securityshopzim.securityshop.util.Util;

/**
 * Created by Framdev1 on 18/11/2016.
 * Auth with security shop pin
 */

public class FragmentAuth extends DialogFragment {


    TextInputLayout layoutPin, layoutPassword;
    LinearLayout progressBar;
    TextInputEditText editPin, editPassword;
    int op = 0;

    FirebaseDatabase database;
    PrefManager pref;

    public static FragmentAuth newInstance() {
        return new FragmentAuth();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.fragment_dialog);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth, container, false);

        database = Util.getDatabase();
        pref = new PrefManager(getActivity());

        layoutPin = (TextInputLayout) view.findViewById(R.id.layoutPin);
        layoutPassword = (TextInputLayout) view.findViewById(R.id.layoutPassword);

        progressBar = (LinearLayout) view.findViewById(R.id.progressLayout);

        editPassword = (TextInputEditText) view.findViewById(R.id.password);
        editPin = (TextInputEditText) view.findViewById(R.id.editPin);

        view.findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                op = -1;

                authenticatePIN();
            }
        });
        view.findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), RegisterActivity.class));
                getActivity().finish();
            }
        });

        return view;
    }

    private void authenticatePIN() {
        final String pin = editPin.getText().toString();
        final String password = editPassword.getText().toString();

        int result = 0;
        if (TextUtils.isEmpty(pin)) {
            result++;
            layoutPin.setError("Security shop PIN required!");
        }
        if (TextUtils.isEmpty(password)) {
            result++;
            layoutPassword.setError("Password required");
        }
        if (result != 0) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        DatabaseReference ref = database.getReference(AppConfig.REF_USER_AUTH);
        Query query = ref.orderByChild("pin").equalTo(pin);
        query.keepSynced(true);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.GONE);
                if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        UserAuth auth = child.getValue(UserAuth.class);
                        String pass = auth.getPassword();
                        if (pass == null) {
                            showNotFoundAlert();
                        } else {
                            pref.savePin(pin);
                            if (pass.equals(password)) {
                                int type = AppConfig.getUserType(pin);
                                Intent intent;
                                if (type == AppConfig.USER_ADMIN) {
                                    intent = new Intent(getActivity(), AdminActivity.class);
                                } else if (type == AppConfig.USER_AGENT) {
                                    intent = new Intent(getActivity(), EmergencyActivity.class);
                                    intent.putExtra("start", type);
                                } else if (type == AppConfig.USER_MEDICAL) {
                                    intent = new Intent(getActivity(), EmergencyActivity.class);
                                    intent.putExtra("start", type);
                                } else {
                                    intent = new Intent(getActivity(), MainActivity.class);
                                }
                                startActivity(intent);
                                getActivity().finish();
                            } else {
                                showInvalidPasswordAlert();
                            }
                        }
                    }
                } else {
                    showNotFoundAlert();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showInvalidPasswordAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Incorrect password");
        builder.setMessage("The password or PIN number you entered was not correct.\r\n\r\nPlease try again");
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void showNotFoundAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Invalid PIN");
        builder.setMessage("The PIN number you entered did not match any Security Shop PINs.\r\n\r\nPlease check your pin and try again or contact Security shop for help");
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
}
