package com.securityshopzim.securityshop.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.objekt.ResponseAgent;
import com.securityshopzim.securityshop.objekt.UserAuth;
import com.securityshopzim.securityshop.util.Util;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Framdev1 on 23/11/2016.
 */

public class NewAgentFragment extends DialogFragment {

    TextInputLayout layoutAgent, layoutEmail, layoutPhone;
    TextInputEditText editAgent, editEmail, editPhone, editPin;
    RadioButton radioSecurity, radioMedical;

    FirebaseDatabase database;
    String code;

    public static NewAgentFragment newInstance() {
        return new NewAgentFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.fragment_dialog);
    }

    private void getCode() {
        DatabaseReference reference = database.getReference(AppConfig.REF_AGENTS);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long count = dataSnapshot.getChildrenCount() + 1;
                code = String.format("%04d", count);
                editPin.setText(code);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_agent, container, false);
        database = Util.getDatabase();

        layoutAgent = (TextInputLayout) view.findViewById(R.id.layoutName);
        layoutEmail = (TextInputLayout) view.findViewById(R.id.layoutEmail);
        layoutPhone = (TextInputLayout) view.findViewById(R.id.layoutPhone);

        editAgent = (TextInputEditText) view.findViewById(R.id.edit_name);
        editEmail = (TextInputEditText) view.findViewById(R.id.edit_email);
        editPhone = (TextInputEditText) view.findViewById(R.id.edit_phone);
        editPhone = (TextInputEditText) view.findViewById(R.id.edit_shop_id);
        editPin = (TextInputEditText) view.findViewById(R.id.edit_shop_id);

        radioSecurity = (RadioButton) view.findViewById(R.id.roleEmergency);
        radioMedical = (RadioButton) view.findViewById(R.id.roleMedical);

        getCode();

        view.findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNewAgent();
            }
        });


        return view;
    }

    private void saveNewAgent() {
        final String name = editAgent.getText().toString();
        final String email = editEmail.getText().toString();
        final String phone = editPhone.getText().toString();
        int type = AppConfig.TYPE_EMERGENCY;
        if (radioMedical.isChecked())
            type = AppConfig.TYPE_MEDICAL;
        if (radioSecurity.isChecked())
            type = AppConfig.TYPE_EMERGENCY;

        int result = 0;
        if (TextUtils.isEmpty(name)) {
            layoutAgent.setError("Please enter agent name");
            result++;
        }
        if (TextUtils.isEmpty(email)) {
            layoutEmail.setError("Please enter email address");
            result++;
        }
        if (TextUtils.isEmpty(phone)) {
            layoutAgent.setError("Please enter mobile number");
            result++;
        }

        if (result > 0)
            return;


        final DatabaseReference ref = database.getReference(AppConfig.REF_AGENTS);
        Query query = ref.orderByChild("email").equalTo(email);
        final int finalType = type;
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {

                    addAuth(email);

                    String uid = ref.push().getKey();

                    ResponseAgent agent = new ResponseAgent(uid, name, code, finalType, email, phone);

                    Map<String, Object> map = agent.toMap();

                    Map<String, Object> child = new HashMap<>();
                    child.put("/" + uid, map);

                    ref.updateChildren(child);

                    Toast.makeText(getActivity(), "New agent created.", Toast.LENGTH_LONG).show();

                    getDialog().dismiss();
                    dismiss();
                } else {
                    Toast.makeText(getActivity(), "Email address already taken", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void addAuth(String email) {
        DatabaseReference ref = database.getReference(AppConfig.REF_USER_AUTH);
        String key = ref.push().getKey();

        if (TextUtils.isEmpty(code))
            code = generateRandom();
        UserAuth auth = new UserAuth(key, code, "default", email);

        Map<String, Object> map = auth.toMap();

        Map<String, Object> child = new HashMap<>();
        child.put("/" + key, map);

        ref.updateChildren(child);
    }

    private String generateRandom() {
        String numbers = "0123456789";

        char[] nums = numbers.toCharArray();
        Random r = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 6; i++) {

            builder.append(nums[r.nextInt(9)]);
        }
        return builder.toString();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
