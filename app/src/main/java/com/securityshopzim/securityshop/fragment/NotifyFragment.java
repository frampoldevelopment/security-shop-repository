package com.securityshopzim.securityshop.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.objekt.NotificationItem;
import com.securityshopzim.securityshop.util.Util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Framdev1 on 23/11/2016.
 */

public class NotifyFragment extends DialogFragment {

    TextInputLayout layoutTitle, layoutNotification;
    TextInputEditText editTitle, editNotification, editCaption;
    FirebaseDatabase database;

    public static NotifyFragment newInstance() {
        return new NotifyFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.fragment_dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notify_users, container, false);
        database = Util.getDatabase();

        layoutNotification = (TextInputLayout) view.findViewById(R.id.layoutNotification);
        layoutTitle = (TextInputLayout) view.findViewById(R.id.layoutTitle);

        editTitle = (TextInputEditText) view.findViewById(R.id.edit_title);
        editNotification = (TextInputEditText) view.findViewById(R.id.edit_email);
        editCaption = (TextInputEditText) view.findViewById(R.id.edit_caption);

        view.findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNotification();
            }
        });


        return view;
    }

    private void saveNotification() {
        String title = editTitle.getText().toString();
        String notification = editNotification.getText().toString();
        String caption = editCaption.getText().toString();

        int result = 0;
        if (TextUtils.isEmpty(title)) {
            layoutTitle.setError("What's this notification about");
            result++;
        }
        if (TextUtils.isEmpty(notification)) {
            layoutNotification.setError("What's the notification");
            result++;
        }

        if (result > 0)
            return;


        DatabaseReference ref = database.getReference(AppConfig.REF_NOTIFICATION);
        String key = ref.push().getKey();

        NotificationItem item = new NotificationItem(key, title, caption, notification, 0, 1);

        Map<String, Object> map = item.toMap();

        Map<String, Object> child = new HashMap<>();
        child.put("/" + key, map);

        ref.updateChildren(child);

        getDialog().dismiss();
        dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
