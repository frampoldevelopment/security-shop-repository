package com.securityshopzim.securityshop.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.securityshopzim.securityshop.R;
import com.securityshopzim.securityshop.activity.MainActivity;
import com.securityshopzim.securityshop.activity.MapsActivity;
import com.securityshopzim.securityshop.activity.SettingsActivity;
import com.securityshopzim.securityshop.adapter.EmergencyCallAdapter;
import com.securityshopzim.securityshop.app.AppConfig;
import com.securityshopzim.securityshop.app.MyApplication;
import com.securityshopzim.securityshop.helper.PrefManager;
import com.securityshopzim.securityshop.objekt.EmergencyCall;
import com.securityshopzim.securityshop.objekt.Wrapper;
import com.securityshopzim.securityshop.util.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Framdev1 on 22/11/2016.
 */

public class EmergencyFragment extends Fragment {
    LinearLayout unAuthorised;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    EmergencyCallAdapter adapter;
    List<EmergencyCall> data;

    FirebaseDatabase database;
    PrefManager pref;
    int type;

    public static EmergencyFragment getInstance() {
        return new EmergencyFragment();
    }

    public static void callIsRead(EmergencyCall call, int read) {
        String uid = call.getUid();
        FirebaseDatabase db = Util.getDatabase();
        DatabaseReference reference = db.getReference(AppConfig.REF_EMERGENCY);
        reference.child(uid).child("read").setValue(read);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_emergency, container, false);
        setHasOptionsMenu(true);
        database = Util.getDatabase();
        pref = new PrefManager(getActivity());

        unAuthorised = (LinearLayout) view.findViewById(R.id.unauthorised);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        data = new ArrayList<>();
        type = getArguments().getInt("type");
        boolean shouldShow = getArguments().getBoolean("shouldShow");
        setViewVisibility(shouldShow);
        firebaseEmergerncyData();
        adapter = new EmergencyCallAdapter(getActivity(), data);
        recyclerView.setAdapter(adapter);


        return view;
    }

    private void setViewVisibility(boolean shouldShow) {
        if (shouldShow) {
            recyclerView.setVisibility(View.VISIBLE);
            unAuthorised.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            unAuthorised.setVisibility(View.VISIBLE);
        }
    }

    public void firebaseEmergerncyData() {
        DatabaseReference reference = database.getReference(AppConfig.REF_EMERGENCY);

        Query query = reference.orderByChild("date");
        query.keepSynced(true);

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                EmergencyCall emergencyCall = dataSnapshot.getValue(EmergencyCall.class);
                if (inThisWindow(emergencyCall) && emergencyCall.getType() == type) {
                    pref.addEmergency(emergencyCall.getUid());
                    adapter.addEmergencyItem(emergencyCall);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                EmergencyCall emergencyCall = dataSnapshot.getValue(EmergencyCall.class);
                if (inThisWindow(emergencyCall) && emergencyCall.getType() == type) {
                    adapter.addEmergencyItem(emergencyCall);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private boolean inThisWindow(EmergencyCall call) {
        try {
            Calendar now = Calendar.getInstance();
            now.add(Calendar.DAY_OF_MONTH, -6);
            Date start = now.getTime();
            Date callDate = (new MyApplication()).formatter().parse(call.getDate());

            return callDate.after(start);

        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_emergency, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_map) {
            Intent intent = new Intent(getActivity(), MapsActivity.class);
            intent.putExtra("wrapper", new Wrapper(data));
            startActivity(intent);
        } else if (item.getItemId() == R.id.action_settings) {
            startActivity(new Intent(getActivity(), SettingsActivity.class));
        } else if (item.getItemId() == R.id.action_emergency) {
            startActivity(new Intent(getActivity(), MainActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
